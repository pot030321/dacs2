﻿namespace DACS.SinhVien
{
    partial class StudentInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            label1 = new Label();
            label2 = new Label();
            label3 = new Label();
            txt_id = new TextBox();
            label4 = new Label();
            txt_name = new TextBox();
            label5 = new Label();
            txt_age = new TextBox();
            label6 = new Label();
            txt_SN = new TextBox();
            label7 = new Label();
            btn_update = new Button();
            btn_find = new Button();
            btn_del = new Button();
            btn_Undel = new Button();
            txt_find = new TextBox();
            label8 = new Label();
            txt_roleid = new TextBox();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Times New Roman", 16.2F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label1.Location = new Point(337, 9);
            label1.Name = "label1";
            label1.Size = new Size(322, 33);
            label1.TabIndex = 0;
            label1.Text = "Thông tin cá nhân sinh viên";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Times New Roman", 12F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label2.Location = new Point(82, 71);
            label2.Name = "label2";
            label2.Size = new Size(30, 22);
            label2.TabIndex = 1;
            label2.Text = "ID";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(53, 117);
            label3.Name = "label3";
            label3.Size = new Size(0, 20);
            label3.TabIndex = 2;
            // 
            // txt_id
            // 
            txt_id.Location = new Point(159, 69);
            txt_id.Name = "txt_id";
            txt_id.Size = new Size(644, 27);
            txt_id.TabIndex = 3;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new Font("Times New Roman", 12F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label4.Location = new Point(59, 131);
            label4.Name = "label4";
            label4.Size = new Size(86, 22);
            label4.TabIndex = 4;
            label4.Text = "Họ và tên";
            // 
            // txt_name
            // 
            txt_name.Location = new Point(159, 129);
            txt_name.Name = "txt_name";
            txt_name.Size = new Size(644, 27);
            txt_name.TabIndex = 5;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new Font("Times New Roman", 12F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label5.Location = new Point(82, 201);
            label5.Name = "label5";
            label5.Size = new Size(46, 22);
            label5.TabIndex = 6;
            label5.Text = "Tuổi";
            // 
            // txt_age
            // 
            txt_age.Location = new Point(159, 199);
            txt_age.Name = "txt_age";
            txt_age.Size = new Size(644, 27);
            txt_age.TabIndex = 7;
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new Font("Times New Roman", 12F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label6.Location = new Point(59, 267);
            label6.Name = "label6";
            label6.Size = new Size(68, 22);
            label6.TabIndex = 8;
            label6.Text = "Trường";
            // 
            // txt_SN
            // 
            txt_SN.Location = new Point(159, 267);
            txt_SN.Name = "txt_SN";
            txt_SN.Size = new Size(644, 27);
            txt_SN.TabIndex = 9;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Font = new Font("Times New Roman", 12F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label7.Location = new Point(59, 332);
            label7.Name = "label7";
            label7.Size = new Size(0, 22);
            label7.TabIndex = 10;
            // 
            // btn_update
            // 
            btn_update.Location = new Point(337, 407);
            btn_update.Margin = new Padding(3, 4, 3, 4);
            btn_update.Name = "btn_update";
            btn_update.Size = new Size(99, 61);
            btn_update.TabIndex = 11;
            btn_update.Text = "Cập Nhật";
            btn_update.UseVisualStyleBackColor = true;
            btn_update.Click += button1_Click;
            // 
            // btn_find
            // 
            btn_find.Location = new Point(707, 404);
            btn_find.Margin = new Padding(3, 4, 3, 4);
            btn_find.Name = "btn_find";
            btn_find.Size = new Size(96, 64);
            btn_find.TabIndex = 11;
            btn_find.Text = "Find";
            btn_find.UseVisualStyleBackColor = true;
            btn_find.Click += btn_find_Click;
            // 
            // btn_del
            // 
            btn_del.Location = new Point(558, 404);
            btn_del.Margin = new Padding(3, 4, 3, 4);
            btn_del.Name = "btn_del";
            btn_del.RightToLeft = RightToLeft.No;
            btn_del.Size = new Size(89, 64);
            btn_del.TabIndex = 12;
            btn_del.Text = "Delete";
            btn_del.UseVisualStyleBackColor = true;
            btn_del.Click += btn_del_Click;
            // 
            // btn_Undel
            // 
            btn_Undel.Location = new Point(159, 407);
            btn_Undel.Margin = new Padding(3, 4, 3, 4);
            btn_Undel.Name = "btn_Undel";
            btn_Undel.Size = new Size(93, 61);
            btn_Undel.TabIndex = 13;
            btn_Undel.Text = "Undel";
            btn_Undel.UseVisualStyleBackColor = true;
            btn_Undel.Click += btn_Undel_Click;
            // 
            // txt_find
            // 
            txt_find.Location = new Point(837, 441);
            txt_find.Margin = new Padding(3, 4, 3, 4);
            txt_find.Name = "txt_find";
            txt_find.Size = new Size(114, 27);
            txt_find.TabIndex = 14;
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Font = new Font("Times New Roman", 12F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label8.Location = new Point(53, 332);
            label8.Name = "label8";
            label8.Size = new Size(103, 22);
            label8.TabIndex = 15;
            label8.Text = "Mã chức vụ";
            // 
            // txt_roleid
            // 
            txt_roleid.Location = new Point(159, 332);
            txt_roleid.Name = "txt_roleid";
            txt_roleid.Size = new Size(644, 27);
            txt_roleid.TabIndex = 16;
            // 
            // StudentInfo
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(997, 504);
            Controls.Add(txt_roleid);
            Controls.Add(label8);
            Controls.Add(txt_find);
            Controls.Add(btn_Undel);
            Controls.Add(btn_del);
            Controls.Add(btn_find);
            Controls.Add(btn_update);
            Controls.Add(label7);
            Controls.Add(txt_SN);
            Controls.Add(label6);
            Controls.Add(txt_age);
            Controls.Add(label5);
            Controls.Add(txt_name);
            Controls.Add(label4);
            Controls.Add(txt_id);
            Controls.Add(label3);
            Controls.Add(label2);
            Controls.Add(label1);
            Name = "StudentInfo";
            Text = "Form1";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label label1;
        private Label label2;
        private Label label3;
        private TextBox txt_id;
        private Label label4;
        private TextBox txt_name;
        private Label label5;
        private TextBox txt_age;
        private Label label6;
        private TextBox txt_SN;
        private Label label7;
        private Button btn_update;
        private Button btn_find;
        private Button btn_del;
        private Button btn_Undel;
        private TextBox txt_find;
        private Label label8;
        private TextBox txt_roleid;
    }
}