﻿namespace DACS
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            pictureBox1 = new PictureBox();
            label1 = new Label();
            label2 = new Label();
            label4 = new Label();
            txt_account = new TextBox();
            label3 = new Label();
            txt_password = new TextBox();
            button1 = new Button();
            chkRemember = new CheckBox();
            lb_login = new Label();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).BeginInit();
            SuspendLayout();
            // 
            // pictureBox1
            // 
            pictureBox1.Image = Properties.Resources.Ảnh_chụp_màn_hình_2024_04_20_102118;
            pictureBox1.Location = new Point(-1, -1);
            pictureBox1.Margin = new Padding(3, 2, 3, 2);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Size = new Size(872, 416);
            pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox1.TabIndex = 0;
            pictureBox1.TabStop = false;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.BackColor = SystemColors.ActiveBorder;
            label1.BorderStyle = BorderStyle.Fixed3D;
            label1.FlatStyle = FlatStyle.Flat;
            label1.Font = new Font("MS Reference Sans Serif", 15.75F);
            label1.ForeColor = Color.Black;
            label1.Location = new Point(168, 190);
            label1.Name = "label1";
            label1.Size = new Size(112, 28);
            label1.TabIndex = 1;
            label1.Text = "OFFICIAL";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.BackColor = SystemColors.ActiveBorder;
            label2.BorderStyle = BorderStyle.Fixed3D;
            label2.FlatStyle = FlatStyle.Flat;
            label2.Font = new Font("MS Reference Sans Serif", 15.75F, FontStyle.Regular, GraphicsUnit.Point, 0);
            label2.ForeColor = Color.FromArgb(255, 128, 0);
            label2.Location = new Point(129, 238);
            label2.Name = "label2";
            label2.Size = new Size(179, 28);
            label2.TabIndex = 2;
            label2.Text = "TIME MANAGER";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.BackColor = SystemColors.WindowFrame;
            label4.Font = new Font("Times New Roman", 16.2F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label4.ForeColor = Color.FromArgb(255, 128, 0);
            label4.Location = new Point(435, 81);
            label4.Name = "label4";
            label4.Size = new Size(144, 25);
            label4.TabIndex = 3;
            label4.Text = "Account Name";
            // 
            // txt_account
            // 
            txt_account.BackColor = SystemColors.HighlightText;
            txt_account.Location = new Point(435, 115);
            txt_account.Name = "txt_account";
            txt_account.Size = new Size(404, 23);
            txt_account.TabIndex = 4;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.BackColor = SystemColors.WindowFrame;
            label3.Font = new Font("Times New Roman", 16.2F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label3.ForeColor = Color.FromArgb(255, 128, 0);
            label3.Location = new Point(435, 153);
            label3.Name = "label3";
            label3.Size = new Size(96, 25);
            label3.TabIndex = 5;
            label3.Text = "Password";
            // 
            // txt_password
            // 
            txt_password.BackColor = SystemColors.HighlightText;
            txt_password.Location = new Point(435, 190);
            txt_password.Name = "txt_password";
            txt_password.Size = new Size(404, 23);
            txt_password.TabIndex = 6;
            txt_password.TextChanged += textBox1_TextChanged;
            // 
            // button1
            // 
            button1.Font = new Font("Times New Roman", 16.2F, FontStyle.Bold, GraphicsUnit.Point, 163);
            button1.ForeColor = Color.FromArgb(255, 128, 0);
            button1.Location = new Point(570, 256);
            button1.Margin = new Padding(3, 2, 3, 2);
            button1.Name = "button1";
            button1.Size = new Size(160, 34);
            button1.TabIndex = 9;
            button1.Text = "Log in";
            button1.UseVisualStyleBackColor = true;
            button1.Click += button1_Click;
            // 
            // chkRemember
            // 
            chkRemember.AutoSize = true;
            chkRemember.BackColor = Color.FromArgb(0, 0, 64);
            chkRemember.Font = new Font("Times New Roman", 9F, FontStyle.Bold, GraphicsUnit.Point, 163);
            chkRemember.ForeColor = Color.FromArgb(255, 128, 0);
            chkRemember.Location = new Point(435, 224);
            chkRemember.Margin = new Padding(3, 2, 3, 2);
            chkRemember.Name = "chkRemember";
            chkRemember.Size = new Size(100, 19);
            chkRemember.TabIndex = 10;
            chkRemember.Text = "Lưu tài khoản";
            chkRemember.UseVisualStyleBackColor = false;
            chkRemember.CheckedChanged += checkBox3_CheckedChanged;
            // 
            // lb_login
            // 
            lb_login.AutoSize = true;
            lb_login.BackColor = SystemColors.WindowFrame;
            lb_login.Font = new Font("Times New Roman", 12F, FontStyle.Bold, GraphicsUnit.Point, 163);
            lb_login.ForeColor = Color.FromArgb(255, 255, 128);
            lb_login.Location = new Point(518, 366);
            lb_login.Name = "lb_login";
            lb_login.Size = new Size(231, 19);
            lb_login.TabIndex = 11;
            lb_login.Text = "Don't have an account ? Register";
            lb_login.Click += lb_login_Click_1;
            // 
            // Login
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(872, 416);
            Controls.Add(lb_login);
            Controls.Add(chkRemember);
            Controls.Add(button1);
            Controls.Add(txt_password);
            Controls.Add(label3);
            Controls.Add(txt_account);
            Controls.Add(label4);
            Controls.Add(label2);
            Controls.Add(label1);
            Controls.Add(pictureBox1);
            Margin = new Padding(3, 2, 3, 2);
            Name = "Login";
            Text = "Form1";
            Load += Form1_Load;
            ((System.ComponentModel.ISupportInitialize)pictureBox1).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private PictureBox pictureBox1;
        private Label label1;
        private Label label2;
        private Label label4;
        private TextBox txt_account;
        private Label label3;
        private TextBox txt_password;
        private Button button1;
        private CheckBox chkRemember;
        private Label lb_login;
    }
}