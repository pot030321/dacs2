﻿
namespace DACS.SinhVien
{
    partial class MainFormSinhVien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            panel_left = new Panel();
            panel_option = new Panel();
            btn_dangxuat = new Button();
            btn_thoat = new Button();
            btn_option = new Button();
            btn_userinfo = new Button();
            panel_timetable = new Panel();
            btn_bangtkb = new Button();
            btn_themtkb = new Button();
            panel_logo = new Panel();
            panel_event = new Panel();
            btn_nhacevent = new Button();
            btn_eventbixoa = new Button();
            btn_danhsachevent = new Button();
            btn_bosungevent = new Button();
            btn_event = new Button();
            btn_timetable = new Button();
            panel_body = new Panel();
            panel_left.SuspendLayout();
            panel_option.SuspendLayout();
            panel_timetable.SuspendLayout();
            panel_event.SuspendLayout();
            SuspendLayout();
            // 
            // panel_left
            // 
            panel_left.AutoScroll = true;
            panel_left.BackColor = SystemColors.ActiveCaption;
            panel_left.Controls.Add(panel_option);
            panel_left.Controls.Add(btn_option);
            panel_left.Controls.Add(btn_userinfo);
            panel_left.Controls.Add(panel_timetable);
            panel_left.Controls.Add(panel_logo);
            panel_left.Controls.Add(panel_event);
            panel_left.Controls.Add(btn_event);
            panel_left.Controls.Add(btn_timetable);
            panel_left.Dock = DockStyle.Left;
            panel_left.Location = new Point(0, 0);
            panel_left.Name = "panel_left";
            panel_left.Size = new Size(182, 572);
            panel_left.TabIndex = 0;
            panel_left.Paint += panel_left_Paint;
            // 
            // panel_option
            // 
            panel_option.Controls.Add(btn_dangxuat);
            panel_option.Controls.Add(btn_thoat);
            panel_option.Location = new Point(4, 433);
            panel_option.Name = "panel_option";
            panel_option.Size = new Size(178, 65);
            panel_option.TabIndex = 13;
            // 
            // btn_dangxuat
            // 
            btn_dangxuat.BackColor = SystemColors.ActiveCaption;
            btn_dangxuat.Dock = DockStyle.Top;
            btn_dangxuat.FlatAppearance.BorderSize = 0;
            btn_dangxuat.FlatStyle = FlatStyle.Flat;
            btn_dangxuat.Font = new Font("Times New Roman", 10.2F, FontStyle.Regular, GraphicsUnit.Point, 163);
            btn_dangxuat.Location = new Point(0, 32);
            btn_dangxuat.Name = "btn_dangxuat";
            btn_dangxuat.Size = new Size(178, 33);
            btn_dangxuat.TabIndex = 9;
            btn_dangxuat.Text = "Đăng xuất";
            btn_dangxuat.UseVisualStyleBackColor = false;
            btn_dangxuat.Click += btn_dangxuat_Click;
            // 
            // btn_thoat
            // 
            btn_thoat.BackColor = SystemColors.ActiveCaption;
            btn_thoat.Dock = DockStyle.Top;
            btn_thoat.FlatAppearance.BorderSize = 0;
            btn_thoat.FlatStyle = FlatStyle.Flat;
            btn_thoat.Font = new Font("Times New Roman", 10.2F, FontStyle.Regular, GraphicsUnit.Point, 163);
            btn_thoat.Location = new Point(0, 0);
            btn_thoat.Name = "btn_thoat";
            btn_thoat.Size = new Size(178, 32);
            btn_thoat.TabIndex = 8;
            btn_thoat.Text = "Thoát";
            btn_thoat.UseVisualStyleBackColor = false;
            btn_thoat.Click += btn_thoat_Click;
            // 
            // btn_option
            // 
            btn_option.BackColor = SystemColors.ActiveCaption;
            btn_option.FlatAppearance.BorderSize = 0;
            btn_option.FlatStyle = FlatStyle.Flat;
            btn_option.Font = new Font("Times New Roman", 10.2F, FontStyle.Bold, GraphicsUnit.Point, 163);
            btn_option.Location = new Point(1, 401);
            btn_option.Name = "btn_option";
            btn_option.Size = new Size(178, 36);
            btn_option.TabIndex = 6;
            btn_option.Text = "Tùy chọn";
            btn_option.UseVisualStyleBackColor = false;
            btn_option.Click += btn_option_Click;
            // 
            // btn_userinfo
            // 
            btn_userinfo.BackColor = SystemColors.ActiveCaption;
            btn_userinfo.FlatAppearance.BorderSize = 0;
            btn_userinfo.FlatStyle = FlatStyle.Flat;
            btn_userinfo.Font = new Font("Times New Roman", 10.2F, FontStyle.Bold, GraphicsUnit.Point, 163);
            btn_userinfo.Location = new Point(3, 359);
            btn_userinfo.Name = "btn_userinfo";
            btn_userinfo.Size = new Size(178, 36);
            btn_userinfo.TabIndex = 5;
            btn_userinfo.Text = "Thông tin cá nhân";
            btn_userinfo.UseVisualStyleBackColor = false;
            btn_userinfo.Click += btn_userinfo_Click;
            // 
            // panel_timetable
            // 
            panel_timetable.Controls.Add(btn_bangtkb);
            panel_timetable.Controls.Add(btn_themtkb);
            panel_timetable.Location = new Point(3, 288);
            panel_timetable.Name = "panel_timetable";
            panel_timetable.Size = new Size(178, 65);
            panel_timetable.TabIndex = 12;
            // 
            // btn_bangtkb
            // 
            btn_bangtkb.BackColor = SystemColors.ActiveCaption;
            btn_bangtkb.Dock = DockStyle.Top;
            btn_bangtkb.FlatAppearance.BorderSize = 0;
            btn_bangtkb.FlatStyle = FlatStyle.Flat;
            btn_bangtkb.Font = new Font("Times New Roman", 10.2F, FontStyle.Regular, GraphicsUnit.Point, 163);
            btn_bangtkb.Location = new Point(0, 32);
            btn_bangtkb.Name = "btn_bangtkb";
            btn_bangtkb.Size = new Size(178, 33);
            btn_bangtkb.TabIndex = 9;
            btn_bangtkb.Text = "Bảng thời khóa biểu";
            btn_bangtkb.UseVisualStyleBackColor = false;
            btn_bangtkb.Click += btn_bangtkb_Click;
            // 
            // btn_themtkb
            // 
            btn_themtkb.BackColor = SystemColors.ActiveCaption;
            btn_themtkb.Dock = DockStyle.Top;
            btn_themtkb.FlatAppearance.BorderSize = 0;
            btn_themtkb.FlatStyle = FlatStyle.Flat;
            btn_themtkb.Font = new Font("Times New Roman", 10.2F, FontStyle.Regular, GraphicsUnit.Point, 163);
            btn_themtkb.Location = new Point(0, 0);
            btn_themtkb.Name = "btn_themtkb";
            btn_themtkb.Size = new Size(178, 32);
            btn_themtkb.TabIndex = 8;
            btn_themtkb.Text = "Thêm thời khóa biểu";
            btn_themtkb.UseVisualStyleBackColor = false;
            btn_themtkb.Click += btn_themtkb_Click;
            // 
            // panel_logo
            // 
            panel_logo.Dock = DockStyle.Top;
            panel_logo.Location = new Point(0, 0);
            panel_logo.Name = "panel_logo";
            panel_logo.Size = new Size(182, 79);
            panel_logo.TabIndex = 8;
            panel_logo.Paint += panel_logo_Paint_1;
            // 
            // panel_event
            // 
            panel_event.Controls.Add(btn_nhacevent);
            panel_event.Controls.Add(btn_eventbixoa);
            panel_event.Controls.Add(btn_danhsachevent);
            panel_event.Controls.Add(btn_bosungevent);
            panel_event.Location = new Point(1, 114);
            panel_event.Name = "panel_event";
            panel_event.Size = new Size(178, 128);
            panel_event.TabIndex = 7;
            // 
            // btn_nhacevent
            // 
            btn_nhacevent.BackColor = SystemColors.ActiveCaption;
            btn_nhacevent.Dock = DockStyle.Top;
            btn_nhacevent.FlatAppearance.BorderSize = 0;
            btn_nhacevent.FlatStyle = FlatStyle.Flat;
            btn_nhacevent.Font = new Font("Times New Roman", 10.2F, FontStyle.Regular, GraphicsUnit.Point, 163);
            btn_nhacevent.Location = new Point(0, 96);
            btn_nhacevent.Name = "btn_nhacevent";
            btn_nhacevent.Size = new Size(178, 32);
            btn_nhacevent.TabIndex = 11;
            btn_nhacevent.Text = "Nhắc nhở";
            btn_nhacevent.UseVisualStyleBackColor = false;
            btn_nhacevent.Click += btn_nhacevent_Click;
            // 
            // btn_eventbixoa
            // 
            btn_eventbixoa.BackColor = SystemColors.ActiveCaption;
            btn_eventbixoa.Dock = DockStyle.Top;
            btn_eventbixoa.FlatAppearance.BorderSize = 0;
            btn_eventbixoa.FlatStyle = FlatStyle.Flat;
            btn_eventbixoa.Font = new Font("Times New Roman", 10.2F, FontStyle.Regular, GraphicsUnit.Point, 163);
            btn_eventbixoa.Location = new Point(0, 64);
            btn_eventbixoa.Name = "btn_eventbixoa";
            btn_eventbixoa.Size = new Size(178, 32);
            btn_eventbixoa.TabIndex = 10;
            btn_eventbixoa.Text = "Sự kiện bị xóa";
            btn_eventbixoa.UseVisualStyleBackColor = false;
            btn_eventbixoa.Click += button3_Click;
            // 
            // btn_danhsachevent
            // 
            btn_danhsachevent.BackColor = SystemColors.ActiveCaption;
            btn_danhsachevent.Dock = DockStyle.Top;
            btn_danhsachevent.FlatAppearance.BorderSize = 0;
            btn_danhsachevent.FlatStyle = FlatStyle.Flat;
            btn_danhsachevent.Font = new Font("Times New Roman", 10.2F, FontStyle.Regular, GraphicsUnit.Point, 163);
            btn_danhsachevent.Location = new Point(0, 32);
            btn_danhsachevent.Name = "btn_danhsachevent";
            btn_danhsachevent.Size = new Size(178, 32);
            btn_danhsachevent.TabIndex = 9;
            btn_danhsachevent.Text = "Danh sách sự kiện";
            btn_danhsachevent.UseVisualStyleBackColor = false;
            btn_danhsachevent.Click += btn_danhsachevent_Click;
            // 
            // btn_bosungevent
            // 
            btn_bosungevent.BackColor = SystemColors.ActiveCaption;
            btn_bosungevent.Dock = DockStyle.Top;
            btn_bosungevent.FlatAppearance.BorderSize = 0;
            btn_bosungevent.FlatStyle = FlatStyle.Flat;
            btn_bosungevent.Font = new Font("Times New Roman", 10.2F, FontStyle.Regular, GraphicsUnit.Point, 163);
            btn_bosungevent.Location = new Point(0, 0);
            btn_bosungevent.Name = "btn_bosungevent";
            btn_bosungevent.Size = new Size(178, 32);
            btn_bosungevent.TabIndex = 8;
            btn_bosungevent.Text = "Thêm sự kiện";
            btn_bosungevent.UseVisualStyleBackColor = false;
            btn_bosungevent.Click += btn_themevent_Click;
            // 
            // btn_event
            // 
            btn_event.BackColor = SystemColors.ActiveCaption;
            btn_event.FlatAppearance.BorderSize = 0;
            btn_event.FlatStyle = FlatStyle.Flat;
            btn_event.Font = new Font("Times New Roman", 10.2F, FontStyle.Bold, GraphicsUnit.Point, 163);
            btn_event.Location = new Point(0, 75);
            btn_event.Name = "btn_event";
            btn_event.Size = new Size(179, 42);
            btn_event.TabIndex = 2;
            btn_event.Text = "Sự kiện";
            btn_event.UseVisualStyleBackColor = false;
            btn_event.Click += btn_event_Click;
            // 
            // btn_timetable
            // 
            btn_timetable.BackColor = SystemColors.ActiveCaption;
            btn_timetable.FlatAppearance.BorderSize = 0;
            btn_timetable.FlatStyle = FlatStyle.Flat;
            btn_timetable.Font = new Font("Times New Roman", 10.2F, FontStyle.Bold, GraphicsUnit.Point, 163);
            btn_timetable.Location = new Point(1, 248);
            btn_timetable.Name = "btn_timetable";
            btn_timetable.Size = new Size(178, 34);
            btn_timetable.TabIndex = 3;
            btn_timetable.Text = "Thời khóa biểu";
            btn_timetable.UseVisualStyleBackColor = false;
            btn_timetable.Click += btn_timetable_Click;
            // 
            // panel_body
            // 
            panel_body.Dock = DockStyle.Fill;
            panel_body.Location = new Point(182, 0);
            panel_body.Name = "panel_body";
            panel_body.Size = new Size(810, 572);
            panel_body.TabIndex = 2;
            panel_body.Paint += panel_body_Paint;
            // 
            // MainFormSinhVien
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(992, 572);
            Controls.Add(panel_body);
            Controls.Add(panel_left);
            Name = "MainFormSinhVien";
            Text = "MainForm";
            Load += MainFormSinhVien_Load;
            panel_left.ResumeLayout(false);
            panel_option.ResumeLayout(false);
            panel_timetable.ResumeLayout(false);
            panel_event.ResumeLayout(false);
            ResumeLayout(false);
        }



        #endregion

        private Panel panel_left;
        private Panel panel_body;
        private Button btn_home;
        private Button btn_userinfo;
        private Button btn_timetable;
        private Button btn_event;
        private Button btn_option;
        private Panel panel_event;
        private Button btn_eventbixoa;
        private Button btn_danhsachevent;
        private Button btn_bosungevent;
        private Button btn_nhacevent;
        private Panel panel_logo;
        private Panel panel_timetable;
        private Button btn_bangtkb;
        private Button btn_themtkb;
        private Panel panel_option;
        private Button btn_dangxuat;
        private Button btn_thoat;
    }
}