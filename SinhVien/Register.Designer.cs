﻿namespace DACS
{
    partial class Register
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            label1 = new Label();
            label2 = new Label();
            label4 = new Label();
            lb_login = new Label();
            label3 = new Label();
            label5 = new Label();
            label6 = new Label();
            txt_account = new TextBox();
            txt_mail = new TextBox();
            txt_pass = new TextBox();
            txt_confirm = new TextBox();
            btn_create = new Button();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.BackColor = SystemColors.ActiveBorder;
            label1.BorderStyle = BorderStyle.Fixed3D;
            label1.FlatStyle = FlatStyle.Flat;
            label1.Font = new Font("MS Reference Sans Serif", 15.75F);
            label1.ForeColor = Color.Black;
            label1.Location = new Point(174, 205);
            label1.Name = "label1";
            label1.Size = new Size(112, 28);
            label1.TabIndex = 0;
            label1.Text = "OFFICIAL";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.BackColor = SystemColors.ActiveBorder;
            label2.BorderStyle = BorderStyle.Fixed3D;
            label2.FlatStyle = FlatStyle.Flat;
            label2.Font = new Font("MS Reference Sans Serif", 15.75F, FontStyle.Regular, GraphicsUnit.Point, 0);
            label2.ForeColor = Color.FromArgb(255, 128, 0);
            label2.Location = new Point(142, 251);
            label2.Name = "label2";
            label2.Size = new Size(179, 28);
            label2.TabIndex = 1;
            label2.Text = "TIME MANAGER";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.BackColor = SystemColors.WindowFrame;
            label4.Font = new Font("Times New Roman", 16.2F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label4.ForeColor = Color.FromArgb(255, 128, 0);
            label4.Location = new Point(451, 60);
            label4.Name = "label4";
            label4.Size = new Size(144, 25);
            label4.TabIndex = 2;
            label4.Text = "Account Name";
            // 
            // lb_login
            // 
            lb_login.AutoSize = true;
            lb_login.BackColor = SystemColors.WindowFrame;
            lb_login.Font = new Font("Times New Roman", 12F, FontStyle.Bold, GraphicsUnit.Point, 163);
            lb_login.ForeColor = Color.FromArgb(255, 255, 128);
            lb_login.Location = new Point(542, 404);
            lb_login.Name = "lb_login";
            lb_login.Size = new Size(227, 19);
            lb_login.TabIndex = 2;
            lb_login.Text = "Already have an account? Log in";
            lb_login.Click += label2_Click;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.BackColor = SystemColors.WindowFrame;
            label3.Font = new Font("Times New Roman", 16.2F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label3.ForeColor = Color.FromArgb(255, 128, 0);
            label3.Location = new Point(451, 191);
            label3.Name = "label3";
            label3.Size = new Size(96, 25);
            label3.TabIndex = 2;
            label3.Text = "Password";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.BackColor = SystemColors.WindowFrame;
            label5.Font = new Font("Times New Roman", 16.2F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label5.ForeColor = Color.FromArgb(255, 128, 0);
            label5.Location = new Point(451, 125);
            label5.Name = "label5";
            label5.Size = new Size(53, 25);
            label5.TabIndex = 2;
            label5.Text = "Mail";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.BackColor = SystemColors.WindowFrame;
            label6.Font = new Font("Times New Roman", 16.2F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label6.ForeColor = Color.FromArgb(255, 128, 0);
            label6.Location = new Point(451, 254);
            label6.Name = "label6";
            label6.Size = new Size(174, 25);
            label6.TabIndex = 2;
            label6.Text = "Confirm Password";
            // 
            // txt_account
            // 
            txt_account.BackColor = SystemColors.HighlightText;
            txt_account.Location = new Point(451, 88);
            txt_account.Name = "txt_account";
            txt_account.Size = new Size(404, 23);
            txt_account.TabIndex = 3;
            // 
            // txt_mail
            // 
            txt_mail.Location = new Point(451, 153);
            txt_mail.Name = "txt_mail";
            txt_mail.Size = new Size(404, 23);
            txt_mail.TabIndex = 4;
            // 
            // txt_pass
            // 
            txt_pass.Location = new Point(451, 219);
            txt_pass.Name = "txt_pass";
            txt_pass.Size = new Size(404, 23);
            txt_pass.TabIndex = 5;
            // 
            // txt_confirm
            // 
            txt_confirm.Location = new Point(451, 282);
            txt_confirm.Name = "txt_confirm";
            txt_confirm.Size = new Size(404, 23);
            txt_confirm.TabIndex = 6;
            // 
            // btn_create
            // 
            btn_create.BackColor = SystemColors.HighlightText;
            btn_create.Font = new Font("Times New Roman", 16.2F, FontStyle.Bold, GraphicsUnit.Point, 163);
            btn_create.ForeColor = Color.FromArgb(255, 128, 0);
            btn_create.Location = new Point(572, 315);
            btn_create.Name = "btn_create";
            btn_create.Size = new Size(188, 45);
            btn_create.TabIndex = 7;
            btn_create.Text = "Create Account";
            btn_create.UseVisualStyleBackColor = false;
            btn_create.Click += btn_create_Click;
            // 
            // Register
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = SystemColors.ActiveCaptionText;
            BackgroundImage = Properties.Resources._6ab994ef_e28f_4b91_94f2_146ee6bd098e;
            BackgroundImageLayout = ImageLayout.Stretch;
            ClientSize = new Size(886, 450);
            Controls.Add(btn_create);
            Controls.Add(txt_confirm);
            Controls.Add(txt_pass);
            Controls.Add(txt_mail);
            Controls.Add(txt_account);
            Controls.Add(lb_login);
            Controls.Add(label5);
            Controls.Add(label6);
            Controls.Add(label3);
            Controls.Add(label4);
            Controls.Add(label2);
            Controls.Add(label1);
            FormBorderStyle = FormBorderStyle.None;
            Name = "Register";
            StartPosition = FormStartPosition.CenterScreen;
            Load += Form1_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label label1;
        private Label label2;
        private Label label4;
        private Label lb_login;
        private Label label3;
        private Label label5;
        private Label label6;
        private TextBox txt_account;
        private TextBox txt_mail;
        private TextBox txt_pass;
        private TextBox txt_confirm;
        private Button btn_create;
    }
}
