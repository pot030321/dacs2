﻿namespace DACS.SinhVien
{
    partial class Events
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            label1 = new Label();
            label2 = new Label();
            label3 = new Label();
            label4 = new Label();
            label5 = new Label();
            label6 = new Label();
            label7 = new Label();
            txt_tenevent = new TextBox();
            label8 = new Label();
            txt_note = new TextBox();
            btn_luu = new Button();
            btn_listevent = new Button();
            txt_uutien = new TextBox();
            label9 = new Label();
            txt_maevent = new TextBox();
            txt_loaievent = new TextBox();
            dateTimePickerStart = new DateTimePicker();
            dateTimePickerEnd = new DateTimePicker();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Times New Roman", 16.2F, FontStyle.Bold, GraphicsUnit.Point, 163);
            label1.Location = new Point(265, 32);
            label1.Name = "label1";
            label1.Size = new Size(141, 25);
            label1.TabIndex = 0;
            label1.Text = "Thêm sự kiện";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Times New Roman", 12F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label2.Location = new Point(13, 142);
            label2.Name = "label2";
            label2.Size = new Size(84, 19);
            label2.TabIndex = 1;
            label2.Text = "Tên sự kiện ";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new Font("Times New Roman", 12F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label3.Location = new Point(13, 175);
            label3.Name = "label3";
            label3.Size = new Size(84, 19);
            label3.TabIndex = 2;
            label3.Text = "Giờ bắt đầu ";
            label3.Click += label3_Click;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new Font("Times New Roman", 12F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label4.Location = new Point(13, 216);
            label4.Name = "label4";
            label4.Size = new Size(83, 19);
            label4.TabIndex = 3;
            label4.Text = "Giờ kết thúc";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new Font("Times New Roman", 12F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label5.Location = new Point(13, 254);
            label5.Name = "label5";
            label5.Size = new Size(84, 19);
            label5.TabIndex = 4;
            label5.Text = "Loại sự kiện";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new Font("Times New Roman", 12F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label6.Location = new Point(13, 286);
            label6.Name = "label6";
            label6.Size = new Size(77, 19);
            label6.TabIndex = 5;
            label6.Text = "Độ ưu tiên ";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Font = new Font("Times New Roman", 12F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label7.Location = new Point(21, 254);
            label7.Name = "label7";
            label7.Size = new Size(0, 19);
            label7.TabIndex = 6;
            // 
            // txt_tenevent
            // 
            txt_tenevent.Location = new Point(112, 138);
            txt_tenevent.Margin = new Padding(3, 2, 3, 2);
            txt_tenevent.Name = "txt_tenevent";
            txt_tenevent.Size = new Size(542, 23);
            txt_tenevent.TabIndex = 7;
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Font = new Font("Times New Roman", 12F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label8.Location = new Point(13, 323);
            label8.Name = "label8";
            label8.Size = new Size(55, 19);
            label8.TabIndex = 12;
            label8.Text = "Ghi chú";
            // 
            // txt_note
            // 
            txt_note.Location = new Point(112, 322);
            txt_note.Margin = new Padding(3, 2, 3, 2);
            txt_note.Name = "txt_note";
            txt_note.Size = new Size(542, 23);
            txt_note.TabIndex = 13;
            // 
            // btn_luu
            // 
            btn_luu.Location = new Point(68, 454);
            btn_luu.Margin = new Padding(3, 2, 3, 2);
            btn_luu.Name = "btn_luu";
            btn_luu.Size = new Size(98, 27);
            btn_luu.TabIndex = 14;
            btn_luu.Text = "Lưu";
            btn_luu.UseVisualStyleBackColor = true;
            btn_luu.Click += btn_luu_Click;
            // 
            // btn_listevent
            // 
            btn_listevent.Location = new Point(495, 454);
            btn_listevent.Margin = new Padding(3, 2, 3, 2);
            btn_listevent.Name = "btn_listevent";
            btn_listevent.Size = new Size(132, 27);
            btn_listevent.TabIndex = 16;
            btn_listevent.Text = "Danh sách đã lưu";
            btn_listevent.UseVisualStyleBackColor = true;
            btn_listevent.Click += btn_listevent_Click;
            // 
            // txt_uutien
            // 
            txt_uutien.Location = new Point(112, 285);
            txt_uutien.Margin = new Padding(3, 2, 3, 2);
            txt_uutien.Name = "txt_uutien";
            txt_uutien.Size = new Size(542, 23);
            txt_uutien.TabIndex = 17;
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Font = new Font("Times New Roman", 12F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label9.Location = new Point(14, 109);
            label9.Name = "label9";
            label9.Size = new Size(82, 19);
            label9.TabIndex = 18;
            label9.Text = "Mã sự kiện ";
            // 
            // txt_maevent
            // 
            txt_maevent.Location = new Point(112, 109);
            txt_maevent.Margin = new Padding(3, 2, 3, 2);
            txt_maevent.Name = "txt_maevent";
            txt_maevent.Size = new Size(542, 23);
            txt_maevent.TabIndex = 19;
            // 
            // txt_loaievent
            // 
            txt_loaievent.Location = new Point(112, 253);
            txt_loaievent.Margin = new Padding(3, 2, 3, 2);
            txt_loaievent.Name = "txt_loaievent";
            txt_loaievent.Size = new Size(542, 23);
            txt_loaievent.TabIndex = 20;
            // 
            // dateTimePickerStart
            // 
            dateTimePickerStart.CustomFormat = "\"dd/MM/yyyy HH:mm:ss\"";
            dateTimePickerStart.Format = DateTimePickerFormat.Custom;
            dateTimePickerStart.Location = new Point(112, 175);
            dateTimePickerStart.Name = "dateTimePickerStart";
            dateTimePickerStart.Size = new Size(542, 23);
            dateTimePickerStart.TabIndex = 21;
            dateTimePickerStart.ValueChanged += dateTimePickerStart_ValueChanged;
            // 
            // dateTimePickerEnd
            // 
            dateTimePickerEnd.CustomFormat = "\"dd/MM/yyyy HH:mm:ss\"";
            dateTimePickerEnd.Format = DateTimePickerFormat.Custom;
            dateTimePickerEnd.Location = new Point(112, 212);
            dateTimePickerEnd.Name = "dateTimePickerEnd";
            dateTimePickerEnd.Size = new Size(542, 23);
            dateTimePickerEnd.TabIndex = 22;
            // 
            // Events
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(700, 502);
            Controls.Add(dateTimePickerEnd);
            Controls.Add(dateTimePickerStart);
            Controls.Add(txt_loaievent);
            Controls.Add(txt_maevent);
            Controls.Add(label9);
            Controls.Add(txt_uutien);
            Controls.Add(btn_listevent);
            Controls.Add(btn_luu);
            Controls.Add(txt_note);
            Controls.Add(label8);
            Controls.Add(txt_tenevent);
            Controls.Add(label7);
            Controls.Add(label6);
            Controls.Add(label5);
            Controls.Add(label4);
            Controls.Add(label3);
            Controls.Add(label2);
            Controls.Add(label1);
            Margin = new Padding(3, 2, 3, 2);
            Name = "Events";
            Text = "Event";
            Load += Form1_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private TextBox txt_tenevent;
        private Label label8;
        private TextBox txt_note;
        private TextBox textBox6;
        private Button button1;
        private ComboBox comboBox1;
        private Button button2;
        private Button btn_luu;
        private Button btn_listevent;
        private TextBox txt_uutien;
        private Label label9;
        private TextBox txt_maevent;
        private TextBox txt_loaievent;
        private DateTimePicker dateTimePickerStart;
        private DateTimePicker dateTimePickerEnd;
    }
}