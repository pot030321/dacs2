﻿using DACS.Models;
using System;
using System.Linq;
using System.Windows.Forms;

namespace DACS.SinhVien
{
    public partial class Timetable : Form
    {
        public Timetable()
        {
            InitializeComponent();
        }

        private bool IsClassSubExists(int classSubId)
        {
            using (var context = new AppDbContext())
            {
                return context.TimeClasses.Any(cs => cs.ClassSubId == classSubId);
            }
        }

        private void AddNewClassSub(int classSubId, string subName, DateTime timeStart, DateTime timeEnd, int subjectId, int classId, int teacherId)
        {
            using (var context = new AppDbContext())
            {
                var newTime_class = new TimeClass
                {

                    SubName = subName,
                    TimeStart = timeStart,
                    TimeEnd = timeEnd,
                    SubJectId = subjectId,
                    Class = classId,
                    teacherId = teacherId
                };

                context.TimeClasses.Add(newTime_class);
                context.SaveChanges();
            }
        }

        public void UpdateClassSub(int classSubId, string subName, DateTime timeStart, DateTime timeEnd, int subjectId, int classId, int teacherId)
        {
            using (var context = new AppDbContext())
            {
                var existingTimeSub = context.TimeClasses.FirstOrDefault(cs => cs.ClassSubId == classSubId);

                if (existingTimeSub != null)
                {
                    existingTimeSub.SubName = subName;
                    existingTimeSub.TimeStart = timeStart;
                    existingTimeSub.TimeEnd = timeEnd;
                    existingTimeSub.SubJectId = subjectId;
                    existingTimeSub.Class = classId;
                    existingTimeSub.teacherId = teacherId;

                    context.SaveChanges();

                    // Update các control trên form
                    txt_malop.Text = classSubId.ToString();
                    txt_tenmon.Text = subName;
                    dateTimePickerStart.Value = timeStart;
                    dateTimePickerEnd.Value = timeEnd;
                    txt_subid.Text = subjectId.ToString();
                    txt_malop.Text = classId.ToString();
                    txt_giaovien.Text = teacherId.ToString();
                }
                else
                {
                    MessageBox.Show("Môn học không tồn tại!");
                }
            }
        }

        private void btn_luutkb_Click(object sender, EventArgs e)
        {
            int classSubId;
            if (int.TryParse(txt_malop.Text, out classSubId))
            {
                string subName = txt_tenmon.Text;
                DateTime timeStart = dateTimePickerStart.Value;
                DateTime timeEnd = dateTimePickerEnd.Value;
                int subjectId;
                if (int.TryParse(txt_subid.Text, out subjectId))
                {
                    int classId;
                    if (int.TryParse(txt_malop.Text, out classId))
                    {
                        int teacherId;
                        if (int.TryParse(txt_giaovien.Text, out teacherId))
                        {
                            if (IsClassSubExists(classSubId))
                            {
                                UpdateClassSub(classSubId, subName, timeStart, timeEnd, subjectId, classId, teacherId);
                                MessageBox.Show("Cập nhật môn học thành công!");
                            }
                            else
                            {
                                AddNewClassSub(classSubId, subName, timeStart, timeEnd, subjectId, classId, teacherId);
                                MessageBox.Show("Thêm mới môn học thành công!");
                            }
                        }
                        else
                        {
                            MessageBox.Show("TeacherId phải là một số nguyên!");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Class không hợp lệ!");
                    }
                }
                else
                {
                    MessageBox.Show("SubjectId không hợp lệ!");
                }
            }
            else
            {
                MessageBox.Show("ClassSubId không hợp lệ!");
            }
        }

        private void btn_thoikhoabieu_Click(object sender, EventArgs e)
        {
            Form f = new TimetableBoard();
            f.ShowDialog();
            this.Hide();
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }
    }
}
