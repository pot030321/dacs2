﻿namespace DACS.SinhVien
{
    partial class Timetable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            label1 = new Label();
            label2 = new Label();
            txt_tenmon = new TextBox();
            label3 = new Label();
            label4 = new Label();
            label5 = new Label();
            btn_luutkb = new Button();
            btn_thoikhoabieu = new Button();
            label6 = new Label();
            txt_giaovien = new TextBox();
            label7 = new Label();
            txt_malop = new TextBox();
            label8 = new Label();
            txt_lop = new TextBox();
            label9 = new Label();
            txt_subid = new TextBox();
            dateTimePickerStart = new DateTimePicker();
            dateTimePickerEnd = new DateTimePicker();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Times New Roman", 19.8000011F, FontStyle.Bold, GraphicsUnit.Point, 163);
            label1.Location = new Point(347, 9);
            label1.Name = "label1";
            label1.Size = new Size(316, 38);
            label1.TabIndex = 0;
            label1.Text = "Nhập thời khóa biểu";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Times New Roman", 13.8F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label2.Location = new Point(69, 116);
            label2.Name = "label2";
            label2.Size = new Size(136, 26);
            label2.TabIndex = 1;
            label2.Text = "Tên môn học";
            // 
            // txt_tenmon
            // 
            txt_tenmon.Location = new Point(225, 117);
            txt_tenmon.Name = "txt_tenmon";
            txt_tenmon.Size = new Size(667, 27);
            txt_tenmon.TabIndex = 2;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new Font("Times New Roman", 13.8F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label3.Location = new Point(101, 179);
            label3.Name = "label3";
            label3.Size = new Size(0, 26);
            label3.TabIndex = 3;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new Font("Times New Roman", 13.8F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label4.Location = new Point(69, 189);
            label4.Name = "label4";
            label4.Size = new Size(126, 26);
            label4.TabIndex = 5;
            label4.Text = "Giờ bắt đầu ";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new Font("Times New Roman", 13.8F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label5.Location = new Point(69, 259);
            label5.Name = "label5";
            label5.Size = new Size(126, 26);
            label5.TabIndex = 7;
            label5.Text = "Giờ kết thúc";
            // 
            // btn_luutkb
            // 
            btn_luutkb.Location = new Point(149, 536);
            btn_luutkb.Name = "btn_luutkb";
            btn_luutkb.Size = new Size(109, 36);
            btn_luutkb.TabIndex = 9;
            btn_luutkb.Text = "Lưu";
            btn_luutkb.UseVisualStyleBackColor = true;
            btn_luutkb.Click += btn_luutkb_Click;
            // 
            // btn_thoikhoabieu
            // 
            btn_thoikhoabieu.Location = new Point(622, 536);
            btn_thoikhoabieu.Name = "btn_thoikhoabieu";
            btn_thoikhoabieu.Size = new Size(253, 36);
            btn_thoikhoabieu.TabIndex = 10;
            btn_thoikhoabieu.Text = "Bảng thời khóa biểu";
            btn_thoikhoabieu.UseVisualStyleBackColor = true;
            btn_thoikhoabieu.Click += btn_thoikhoabieu_Click;
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new Font("Times New Roman", 13.8F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label6.Location = new Point(69, 332);
            label6.Name = "label6";
            label6.Size = new Size(102, 26);
            label6.TabIndex = 11;
            label6.Text = "Giáo viên";
            // 
            // txt_giaovien
            // 
            txt_giaovien.Location = new Point(225, 333);
            txt_giaovien.Name = "txt_giaovien";
            txt_giaovien.Size = new Size(667, 27);
            txt_giaovien.TabIndex = 12;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Font = new Font("Times New Roman", 13.8F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label7.Location = new Point(75, 65);
            label7.Name = "label7";
            label7.Size = new Size(117, 26);
            label7.TabIndex = 13;
            label7.Text = "Mã lớp học";
            // 
            // txt_malop
            // 
            txt_malop.Location = new Point(225, 67);
            txt_malop.Name = "txt_malop";
            txt_malop.Size = new Size(667, 27);
            txt_malop.TabIndex = 14;
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Font = new Font("Times New Roman", 13.8F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label8.Location = new Point(75, 400);
            label8.Name = "label8";
            label8.Size = new Size(49, 26);
            label8.TabIndex = 15;
            label8.Text = "Lớp";
            label8.Click += label8_Click;
            // 
            // txt_lop
            // 
            txt_lop.Location = new Point(225, 401);
            txt_lop.Name = "txt_lop";
            txt_lop.Size = new Size(667, 27);
            txt_lop.TabIndex = 16;
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Font = new Font("Times New Roman", 13.8F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label9.Location = new Point(75, 455);
            label9.Name = "label9";
            label9.Size = new Size(129, 26);
            label9.TabIndex = 17;
            label9.Text = "Mã môn học";
            // 
            // txt_subid
            // 
            txt_subid.Location = new Point(225, 456);
            txt_subid.Name = "txt_subid";
            txt_subid.Size = new Size(667, 27);
            txt_subid.TabIndex = 18;
            // 
            // dateTimePickerStart
            // 
            dateTimePickerStart.CustomFormat = "\"dd/MM/yyyy HH:mm:ss\"";
            dateTimePickerStart.Format = DateTimePickerFormat.Custom;
            dateTimePickerStart.Location = new Point(225, 189);
            dateTimePickerStart.Margin = new Padding(3, 4, 3, 4);
            dateTimePickerStart.Name = "dateTimePickerStart";
            dateTimePickerStart.Size = new Size(667, 27);
            dateTimePickerStart.TabIndex = 22;
            // 
            // dateTimePickerEnd
            // 
            dateTimePickerEnd.CustomFormat = "\"dd/MM/yyyy HH:mm:ss\"";
            dateTimePickerEnd.Format = DateTimePickerFormat.Custom;
            dateTimePickerEnd.Location = new Point(225, 256);
            dateTimePickerEnd.Margin = new Padding(3, 4, 3, 4);
            dateTimePickerEnd.Name = "dateTimePickerEnd";
            dateTimePickerEnd.Size = new Size(667, 27);
            dateTimePickerEnd.TabIndex = 22;
            // 
            // Timetable
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(971, 659);
            Controls.Add(dateTimePickerEnd);
            Controls.Add(dateTimePickerStart);
            Controls.Add(txt_subid);
            Controls.Add(label9);
            Controls.Add(txt_lop);
            Controls.Add(label8);
            Controls.Add(txt_malop);
            Controls.Add(label7);
            Controls.Add(txt_giaovien);
            Controls.Add(label6);
            Controls.Add(btn_thoikhoabieu);
            Controls.Add(btn_luutkb);
            Controls.Add(label5);
            Controls.Add(label4);
            Controls.Add(label3);
            Controls.Add(txt_tenmon);
            Controls.Add(label2);
            Controls.Add(label1);
            Name = "Timetable";
            Text = "Timetable";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label label1;
        private Label label2;
        private TextBox txt_tenmon;
        private Label label3;
        private Label label4;
        private Label label5;
        private Button btn_luutkb;
        private Button btn_thoikhoabieu;
        private Label label6;
        private TextBox txt_giaovien;
        private Label label7;
        private TextBox txt_malop;
        private Label label8;
        private TextBox txt_lop;
        private Label label9;
        private TextBox txt_subid;
        private DateTimePicker dateTimePickerStart;
        private DateTimePicker dateTimePickerEnd;
    }
}