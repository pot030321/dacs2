﻿namespace DACS.SinhVien
{
    partial class EventList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            label1 = new Label();
            dgvEvents = new DataGridView();
            btn_suaevent = new Button();
            btn_themevent = new Button();
            txt_timkiemevent = new TextBox();
            btn_timkiemevent = new Button();
            webView21 = new Microsoft.Web.WebView2.WinForms.WebView2();
            ((System.ComponentModel.ISupportInitialize)dgvEvents).BeginInit();
            ((System.ComponentModel.ISupportInitialize)webView21).BeginInit();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Times New Roman", 19.8000011F, FontStyle.Bold, GraphicsUnit.Point, 163);
            label1.Location = new Point(411, 37);
            label1.Name = "label1";
            label1.Size = new Size(283, 38);
            label1.TabIndex = 0;
            label1.Text = "Danh sách sự kiện";
            label1.Click += label1_Click;
            // 
            // dgvEvents
            // 
            dgvEvents.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvEvents.Location = new Point(75, 190);
            dgvEvents.Name = "dgvEvents";
            dgvEvents.RowHeadersWidth = 51;
            dgvEvents.Size = new Size(929, 188);
            dgvEvents.TabIndex = 1;
            dgvEvents.CellContentClick += dataGridView1_CellContentClick;
            // 
            // btn_suaevent
            // 
            btn_suaevent.Location = new Point(128, 411);
            btn_suaevent.Name = "btn_suaevent";
            btn_suaevent.Size = new Size(153, 45);
            btn_suaevent.TabIndex = 2;
            btn_suaevent.Text = "Sửa";
            btn_suaevent.UseVisualStyleBackColor = true;
            btn_suaevent.Click += btn_suaevent_Click;
            // 
            // btn_themevent
            // 
            btn_themevent.Location = new Point(794, 411);
            btn_themevent.Name = "btn_themevent";
            btn_themevent.Size = new Size(153, 45);
            btn_themevent.TabIndex = 3;
            btn_themevent.Text = "Thêm sự kiện";
            btn_themevent.UseVisualStyleBackColor = true;
            btn_themevent.Click += btn_themevent_Click;
            // 
            // txt_timkiemevent
            // 
            txt_timkiemevent.Location = new Point(672, 123);
            txt_timkiemevent.Name = "txt_timkiemevent";
            txt_timkiemevent.Size = new Size(222, 27);
            txt_timkiemevent.TabIndex = 4;
            // 
            // btn_timkiemevent
            // 
            btn_timkiemevent.Location = new Point(900, 123);
            btn_timkiemevent.Name = "btn_timkiemevent";
            btn_timkiemevent.Size = new Size(104, 29);
            btn_timkiemevent.TabIndex = 6;
            btn_timkiemevent.Text = "Tìm kiếm";
            btn_timkiemevent.UseVisualStyleBackColor = true;
            // 
            // webView21
            // 
            webView21.CreationProperties = null;
            webView21.DefaultBackgroundColor = Color.White;
            webView21.Location = new Point(0, 0);
            webView21.Name = "webView21";
            webView21.Size = new Size(94, 29);
            webView21.TabIndex = 5;
            webView21.ZoomFactor = 1D;
            // 
            // EventList
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1094, 560);
            Controls.Add(btn_timkiemevent);
            Controls.Add(webView21);
            Controls.Add(txt_timkiemevent);
            Controls.Add(btn_themevent);
            Controls.Add(btn_suaevent);
            Controls.Add(dgvEvents);
            Controls.Add(label1);
            Name = "EventList";
            Text = "EventList";
            ((System.ComponentModel.ISupportInitialize)dgvEvents).EndInit();
            ((System.ComponentModel.ISupportInitialize)webView21).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label label1;
        private DataGridView dgvEvents;
        private Button btn_suaevent;
        private Button btn_themevent;
        private TextBox txt_timkiemevent;
        private Button btn_timkiemevent;
        private Microsoft.Web.WebView2.WinForms.WebView2 webView21;
    }
}