﻿using DACS.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DACS.SinhVien
{
    public partial class TimetableBoard : Form
    {
        public TimetableBoard()
        {
            InitializeComponent();
            FillTimeGrid();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        public void FillTimeGrid()
        {
            using (var context = new AppDbContext())
            {
                var TimeClasses = context.TimeClasses.ToList();
                // set data cho dtg
                dataGridViewTimeTable.DataSource = TimeClasses;
                dataGridViewTimeTable.Refresh();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dataGridViewTimeTable.SelectedRows.Count > 0)
            {
                int classSubId = Convert.ToInt32(dataGridViewTimeTable.SelectedRows[0].Cells["ClassSubId"].Value);
                string subName = dataGridViewTimeTable.SelectedRows[0].Cells["SubName"].Value.ToString();
                int subjectId = Convert.ToInt32(dataGridViewTimeTable.SelectedRows[0].Cells["SubJectId"].Value);
                DateTime timeStart = Convert.ToDateTime(dataGridViewTimeTable.SelectedRows[0].Cells["TimeStart"].Value);
                DateTime timeEnd = Convert.ToDateTime(dataGridViewTimeTable.SelectedRows[0].Cells["TimeEnd"].Value);
                int classId = Convert.ToInt32(dataGridViewTimeTable.SelectedRows[0].Cells["Class"].Value);
                int teacherId = Convert.ToInt32(dataGridViewTimeTable.SelectedRows[0].Cells["teacherId"].Value);

                Timetable timetableForm = new Timetable();
                timetableForm.UpdateClassSub(classSubId, subName, timeStart, timeEnd, subjectId, classId, teacherId);
                timetableForm.ShowDialog();

                // gọi hàm để cập nhật dữ liệu 
                FillTimeGrid();
            }
            else
            {
                MessageBox.Show("Vui lòng chọn một môn học để sửa!");
            }

            dataGridViewTimeTable.Refresh();
        }


        private void button2_Click(object sender, EventArgs e)
        {
            Form f = new Timetable();
            f.ShowDialog();
            this.Hide();
        }
    }
}
