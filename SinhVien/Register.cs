﻿using DACS.Models;

namespace DACS
{
    public partial class Register : Form
    {
        public Register()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

            Login f = new Login();
            f.Show();
        }

        private void btn_create_Click(object sender, EventArgs e)
        {
            
            
                string username = txt_account.Text;
                string mail = txt_mail.Text;
                string password = txt_pass.Text;
                string comfirmpass = txt_confirm.Text;

                // Kiểm tra mật khẩu và mật khẩu xác nhận
                if (password != comfirmpass)
                {
                    MessageBox.Show("Mật khẩu xác nhận không khớp!");
                    return; // Dừng thực thi phương thức nếu mật khẩu xác nhận không khớp
                }

                using (var context = new AppDbContext()) // Thay thế bằng tên DbContext của bạn
                {
                    // Kiểm tra xem Username đã tồn tại chưa
                    var existingUser = context.Accounts.FirstOrDefault(u => u.UserName == username);

                    if (existingUser != null)
                    {
                        MessageBox.Show("Tên đăng nhập đã tồn tại!");
                        return; // Dừng thực thi phương thức để ngăn việc thêm người dùng trùng lặp
                    }

                    // Tạo một người dùng mới
                    var newAccount = new Account
                    {
                        UserName = username,
                        Password = password,
                        Mail = mail
                    };

                    // Thêm người dùng mới vào cơ sở dữ liệu
                    context.Accounts.Add(newAccount);
                    context.SaveChanges();

                    MessageBox.Show("Đăng ký thành công!");
                    // Tiến hành cập nhật hoặc chuyển đến form/cửa sổ khác
                }
            }

        }
    }
