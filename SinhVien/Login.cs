﻿using DACS.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DACS
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void lb_login_Click(object sender, EventArgs e)
        {

        }

        private void btn_create_Click(object sender, EventArgs e)
        {

        }
        private const string DirectoryPath = @"C:\Users\HP\Desktop\DACS\DACS-thinh\DACS-phong";
        private const string FileName = "login.txt";
        private void button1_Click(object sender, EventArgs e)
        {
            string username = txt_account.Text;
            string password = txt_password.Text;

            if (chkRemember.Checked)
            {
                // Lưu thông tin đăng nhập vào file
                File.WriteAllText(FileName, $"{username},{password}");
            }
            else
            {
                // Xóa file thông tin đăng nhập
                if (File.Exists(FileName))
                {
                    File.Delete(FileName);
                }
            }
            using (var context = new AppDbContext()) 
            {
                var user = context.Accounts.FirstOrDefault(u => u.UserName == username && u.Password == password);

                if (user != null)
                {
                    MessageBox.Show("Đăng nhập thành công!");
                    // Tiến hành cập nhật hoặc chuyển đến form/cửa sổ khác
                }
                else
                {
                    MessageBox.Show("Tên đăng nhập hoặc mật khẩu không đúng!");
                }
            }
        }
        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void lb_login_Click_1(object sender, EventArgs e)
        {
            Register f = new Register();
            f.Show();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
