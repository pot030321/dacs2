﻿namespace DACS.SinhVien
{
    partial class TimetableBoard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            dataGridViewTimeTable = new DataGridView();
            button1 = new Button();
            button2 = new Button();
            ((System.ComponentModel.ISupportInitialize)dataGridViewTimeTable).BeginInit();
            SuspendLayout();
            // 
            // dataGridViewTimeTable
            // 
            dataGridViewTimeTable.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewTimeTable.Location = new Point(46, 55);
            dataGridViewTimeTable.Margin = new Padding(3, 2, 3, 2);
            dataGridViewTimeTable.Name = "dataGridViewTimeTable";
            dataGridViewTimeTable.RowHeadersWidth = 51;
            dataGridViewTimeTable.Size = new Size(813, 184);
            dataGridViewTimeTable.TabIndex = 0;
            dataGridViewTimeTable.CellContentClick += dataGridView1_CellContentClick;
            // 
            // button1
            // 
            button1.Location = new Point(46, 306);
            button1.Margin = new Padding(3, 2, 3, 2);
            button1.Name = "button1";
            button1.Size = new Size(122, 31);
            button1.TabIndex = 1;
            button1.Text = "Sửa";
            button1.UseVisualStyleBackColor = true;
            button1.Click += button1_Click;
            // 
            // button2
            // 
            button2.Location = new Point(737, 306);
            button2.Margin = new Padding(3, 2, 3, 2);
            button2.Name = "button2";
            button2.Size = new Size(122, 31);
            button2.TabIndex = 2;
            button2.Text = "Thêm lịch học";
            button2.UseVisualStyleBackColor = true;
            button2.Click += button2_Click;
            // 
            // TimetableBoard
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(928, 416);
            Controls.Add(button2);
            Controls.Add(button1);
            Controls.Add(dataGridViewTimeTable);
            Margin = new Padding(3, 2, 3, 2);
            Name = "TimetableBoard";
            Text = "TimetableBoard";
            ((System.ComponentModel.ISupportInitialize)dataGridViewTimeTable).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private DataGridView dataGridViewTimeTable;
        private Button button1;
        private Button button2;
    }
}