﻿using DACS.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace DACS.SinhVien
{
    public partial class Events : Form
    {
        public Events()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private bool IsEventExists(int eventid)
        {
            using (var context = new AppDbContext())
            {
                return context.Events.Any(u => u.EventId == eventid);
            }
        }

        private void AddNewEvent(string name_event, DateTime Time_Start, int evTypeId, DateTime Time_End, string note, int priority)
        {
            using (var context = new AppDbContext())
            {
                // Tạo một sự kiện mới
                var newEvent = new Event
                {
                    EvName = name_event,
                    TimeStart = Time_Start,
                    TimeEnd = Time_End,
                    EventTypeId = evTypeId,
                    Priority = priority,
                    Note = note
                };

                // Thêm sự kiện mới vào DbSet
                context.Events.Add(newEvent);

                // Lưu thay đổi vào cơ sở dữ liệu
                context.SaveChanges();
            }
        }

        public void UpdateEvent(int event_id, string name_event, int evTypeId, DateTime Time_Start, DateTime Time_End, string note, int priority)
        {
            using (var context = new AppDbContext())
            {
                // Tìm sự kiện dựa trên event_id
                var existingEvent = context.Events.FirstOrDefault(e => e.EventId == event_id);

                if (existingEvent != null)
                {
                    // Cập nhật thông tin sự kiện
                    existingEvent.EvName = name_event;
                    existingEvent.TimeStart = Time_Start;
                    existingEvent.TimeEnd = Time_End;
                    existingEvent.EventTypeId = evTypeId;
                    existingEvent.Note = note;
                    existingEvent.Priority = priority;

                    // Lưu thay đổi vào cơ sở dữ liệu
                    context.SaveChanges();

                    // Cập nhật lại các control trên form
                    txt_maevent.Text = event_id.ToString();
                    txt_tenevent.Text = name_event;
                    txt_loaievent.Text = evTypeId.ToString();
                    dateTimePickerStart.Value = Time_Start;
                    dateTimePickerEnd.Value = Time_End;
                    txt_note.Text = note;
                    txt_uutien.Text = priority.ToString();
                }
                else
                {
                    MessageBox.Show("Sự kiện không tồn tại!");
                }
            }
        }




        private void btn_luu_Click(object sender, EventArgs e)
        {
            int event_id;
            if (int.TryParse(txt_maevent.Text, out event_id))
            {
                string name_event = txt_tenevent.Text;
                int evTypeId;
                if (int.TryParse(txt_loaievent.Text, out evTypeId))
                {
                    DateTime Time_Start = dateTimePickerStart.Value;  // Lấy thời gian từ DateTimePicker
                    DateTime Time_End = dateTimePickerEnd.Value;
                    string note = txt_note.Text;  // Lấy note từ textbox

                    int priority;
                    if (int.TryParse(txt_uutien.Text, out priority))
                    {
                        if (IsEventExists(event_id))  // Kiểm tra sự kiện đã tồn tại hay chưa
                        {
                            UpdateEvent(event_id, name_event, evTypeId, Time_Start, Time_End, note, priority);
                            MessageBox.Show("Cập nhật sự kiện thành công!");
                        }
                        else
                        {
                            AddNewEvent(name_event, Time_Start, evTypeId, Time_End, note, priority);
                            MessageBox.Show("Thêm mới sự kiện thành công!");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Ưu tiên phải là một số nguyên!");
                    }
                }
                else
                {
                    MessageBox.Show("Loại sự kiện không hợp lệ!");
                }
            }
            else
            {
                MessageBox.Show("Mã sự kiện không hợp lệ!");
            }

        }

        private void btn_listevent_Click(object sender, EventArgs e)
        {
            Form f = new EventList();
            f.Show();
            this.Hide();
        }

        private void dateTimePickerStart_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
