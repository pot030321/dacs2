﻿using DACS.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace DACS.SinhVien
{
    public partial class StudentInfo : Form
    {
        public StudentInfo()
        {
            InitializeComponent();
        }
        private void UpdateUser(int userId, string name, int age, string school, int role)
        {
            using (var context = new AppDbContext())
            {
                // Tìm người dùng cần cập nhật
                var user = context.Users.FirstOrDefault(u => u.UserID == userId);

                if (user != null)
                {
                    // Cập nhật thông tin người dùng
                    user.Name = name;
                    user.Age = age;
                    user.SN = school;
                    user.RoleId = 1;

                    // Lưu thay đổi vào cơ sở dữ liệu
                    context.SaveChanges();
                }

            }
        }
        private void AddNewUser(string name, int age, string school, int role)
        {
            using (var context = new AppDbContext())
            {
                // Tạo một người dùng mới
                var newUser = new User
                {
                    Name = name,
                    Age = age,
                    SN = school,
                    RoleId = 1
                };

                // Thêm người dùng mới vào DbSet
                context.Users.Add(newUser);

                // Lưu thay đổi vào cơ sở dữ liệu
                context.SaveChanges();


            }
        }

        private bool IsUserExists(int userId)
        {
            using (var context = new AppDbContext())
            {
                return context.Users.Any(u => u.UserID == userId && !u.Delete);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int userId;
            if (int.TryParse(txt_id.Text, out userId))
            {
                string name = txt_name.Text;
                int age;
                if (int.TryParse(txt_age.Text, out age))
                {
                    string school = txt_SN.Text;
                    int role = 1;

                    if (IsUserExists(userId))
                    {
                        UpdateUser(userId, name, age, school, role);
                        MessageBox.Show("Cập Nhật Thành Công người dùng!");
                    }
                    else
                    {
                        AddNewUser(name, age, school, role);
                        MessageBox.Show("Thêm mới người dùng thành công!");
                    }
                }

            }

        }

        private void btn_Undel_Click(object sender, EventArgs e)
        {
            int userId;
            if (int.TryParse(txt_find.Text, out userId))
            {
                UnDeleteUser(userId); // Khôi phục người dùng
                MessageBox.Show("Khôi phục người dùng thành công!");

                // Cập nhật lại thông tin người dùng trên form (nếu cần)
                UpdateUserInfo(userId);
            }
            else
            {
                MessageBox.Show("Vui lòng nhập UserID là một số nguyên!");
            }
        }
        private void UpdateUserInfo(int userId)
        {
            using (var context = new AppDbContext())
            {
                var user = context.Users.FirstOrDefault(u => u.UserID == userId && !u.Delete);

                if (user != null)
                {
                    txt_id.Text = user.UserID.ToString();
                    txt_name.Text = user.Name;
                    txt_age.Text = user.Age.ToString();
                    txt_SN.Text = user.SN;
                    // txtRole.Text = user.Role.ToString();  // Bạn có thể hiển thị Role nếu muốn
                }
            }
        }

        private void UnDeleteUser(int userId)
        {
            using (var context = new AppDbContext())
            {
                var user = context.Users.FirstOrDefault(u => u.UserID == userId && u.Delete);

                if (user != null)
                {
                    user.Delete = false;  // Đặt trường IsDeleted là false để khôi phục người dùng
                    context.SaveChanges();
                }
                else
                {
                    MessageBox.Show("Không tìm thấy người dùng đã xóa!");
                }
            }
        }
        private void DeleteUser(int userId)
        {
            using (var context = new AppDbContext())
            {
                var user = context.Users.FirstOrDefault(u => u.UserID == userId);

                if (user != null)
                {
                    user.Delete = true;  // Đặt trường IsDeleted là true để ẩn người dùng

                    context.SaveChanges();
                    MessageBox.Show("Xóa người dùng thành công!");
                }
                else
                {
                    MessageBox.Show("Không tìm thấy người dùng!");
                }
            }
        }


        private void btn_del_Click(object sender, EventArgs e)
        {
            int userId;
            if (int.TryParse(txt_id.Text, out userId))
            {
                DeleteUser(userId);
                txt_name.Text = "";
                txt_id.Text = "";
                txt_SN.Text = "";
                txt_age.Text = "";

            }
            else
            {
                MessageBox.Show("Vui lòng nhập UserID đúng!");
            }
        }
        private User FindUserById(int userId)
        {
            using (var context = new AppDbContext())
            {
                return context.Users.FirstOrDefault(u => u.UserID == userId && !u.Delete);
            }
        }

        private void btn_find_Click(object sender, EventArgs e)
        {
            int userId;
            if (int.TryParse(txt_find.Text, out userId))
            {
                var user = FindUserById(userId);

                if (user != null)
                {
                    // Hiển thị thông tin người dùng trong các TextBox khác
                    txt_id.Text = user.UserID.ToString();
                    txt_name.Text = user.Name;
                    txt_age.Text = user.Age.ToString();
                    txt_SN.Text = user.SN;
                    // txtRole.Text = user.Role.ToString();  // Bạn có thể hiển thị Role nếu muốn

                    MessageBox.Show("Tìm thấy người dùng!");
                }
                else
                {
                    MessageBox.Show("Không tìm thấy người dùng!");
                }
            }
            else
            {
                MessageBox.Show("Vui lòng nhập UserID đúng!");
            }
        }
    }
}
