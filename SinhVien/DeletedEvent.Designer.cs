﻿namespace DACS.SinhVien
{
    partial class DeletedEvent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            label1 = new Label();
            dataGridView1 = new DataGridView();
            button1 = new Button();
            txt_maeventbixoa = new DataGridViewTextBoxColumn();
            txt_teneventbixoa = new DataGridViewTextBoxColumn();
            txt_timestartbijxoa = new DataGridViewTextBoxColumn();
            txt_gioketthucbixoa = new DataGridViewTextBoxColumn();
            txt_loaieventbixoa = new DataGridViewTextBoxColumn();
            txt_douutienbixoa = new DataGridViewTextBoxColumn();
            txt_notebixoa = new DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)dataGridView1).BeginInit();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Times New Roman", 19.8000011F, FontStyle.Bold, GraphicsUnit.Point, 163);
            label1.Location = new Point(342, 56);
            label1.Name = "label1";
            label1.Size = new Size(379, 38);
            label1.TabIndex = 0;
            label1.Text = "Danh sách sự kiện bị xóa";
            label1.Click += label1_Click;
            // 
            // dataGridView1
            // 
            dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridView1.Columns.AddRange(new DataGridViewColumn[] { txt_maeventbixoa, txt_teneventbixoa, txt_timestartbijxoa, txt_gioketthucbixoa, txt_loaieventbixoa, txt_douutienbixoa, txt_notebixoa });
            dataGridView1.Location = new Point(53, 127);
            dataGridView1.Name = "dataGridView1";
            dataGridView1.RowHeadersWidth = 51;
            dataGridView1.Size = new Size(929, 284);
            dataGridView1.TabIndex = 1;
            // 
            // button1
            // 
            button1.Location = new Point(457, 447);
            button1.Name = "button1";
            button1.Size = new Size(94, 29);
            button1.TabIndex = 2;
            button1.Text = "Hoàn tác";
            button1.UseVisualStyleBackColor = true;
            // 
            // txt_maeventbixoa
            // 
            txt_maeventbixoa.HeaderText = "Mã sự kiện";
            txt_maeventbixoa.MinimumWidth = 6;
            txt_maeventbixoa.Name = "txt_maeventbixoa";
            txt_maeventbixoa.Width = 125;
            // 
            // txt_teneventbixoa
            // 
            txt_teneventbixoa.HeaderText = "Tên sự kiện";
            txt_teneventbixoa.MinimumWidth = 6;
            txt_teneventbixoa.Name = "txt_teneventbixoa";
            txt_teneventbixoa.Width = 125;
            // 
            // txt_timestartbijxoa
            // 
            txt_timestartbijxoa.HeaderText = "Giờ bắt đầu";
            txt_timestartbijxoa.MinimumWidth = 6;
            txt_timestartbijxoa.Name = "txt_timestartbijxoa";
            txt_timestartbijxoa.Width = 125;
            // 
            // txt_gioketthucbixoa
            // 
            txt_gioketthucbixoa.HeaderText = "Giờ kết thúc";
            txt_gioketthucbixoa.MinimumWidth = 6;
            txt_gioketthucbixoa.Name = "txt_gioketthucbixoa";
            txt_gioketthucbixoa.Width = 125;
            // 
            // txt_loaieventbixoa
            // 
            txt_loaieventbixoa.HeaderText = "Loại sự kiện";
            txt_loaieventbixoa.MinimumWidth = 6;
            txt_loaieventbixoa.Name = "txt_loaieventbixoa";
            txt_loaieventbixoa.Width = 125;
            // 
            // txt_douutienbixoa
            // 
            txt_douutienbixoa.HeaderText = "Độ ưu tiên";
            txt_douutienbixoa.MinimumWidth = 6;
            txt_douutienbixoa.Name = "txt_douutienbixoa";
            txt_douutienbixoa.Width = 125;
            // 
            // txt_notebixoa
            // 
            txt_notebixoa.HeaderText = "Ghi chú";
            txt_notebixoa.MinimumWidth = 6;
            txt_notebixoa.Name = "txt_notebixoa";
            txt_notebixoa.Width = 125;
            // 
            // DeletedEvent
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1048, 519);
            Controls.Add(button1);
            Controls.Add(dataGridView1);
            Controls.Add(label1);
            Name = "DeletedEvent";
            Text = "DeletedEvent";
            ((System.ComponentModel.ISupportInitialize)dataGridView1).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label label1;
        private DataGridView dataGridView1;
        private Button button1;
        private DataGridViewTextBoxColumn txt_maeventbixoa;
        private DataGridViewTextBoxColumn txt_teneventbixoa;
        private DataGridViewTextBoxColumn txt_timestartbijxoa;
        private DataGridViewTextBoxColumn txt_gioketthucbixoa;
        private DataGridViewTextBoxColumn txt_loaieventbixoa;
        private DataGridViewTextBoxColumn txt_douutienbixoa;
        private DataGridViewTextBoxColumn txt_notebixoa;
    }
}