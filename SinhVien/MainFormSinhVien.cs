﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DACS.SinhVien
{
    public partial class MainFormSinhVien : Form
    {
        public MainFormSinhVien()
        {
            InitializeComponent();
            CustomizeMenu();
        }
        private Form currentFormChild;
        private void OpenChildForm(Form childForm)
        {
            if (currentFormChild != null)
            {
                currentFormChild.Close();
            }
            currentFormChild = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            panel_body.Controls.Add(childForm);
            panel_body.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
        }
        private void CustomizeMenu()
        {
            panel_event.Visible = false;
            panel_timetable.Visible = false;
            panel_option.Visible = false;
        }
        private void HideMenu()
        {
            if (panel_event.Visible == true)
                panel_event.Visible = false;
            if (panel_timetable.Visible == true)
                panel_timetable.Visible = false;
            if (panel_option.Visible == true)
                panel_option.Visible = false;
        }
        private void ShowMenu(Panel SubMenu)
        {
            if (SubMenu.Visible == false)
            {
                HideMenu();
                SubMenu.Visible = true;
            }
            else
                SubMenu.Visible = false;
        }
        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void btn_home_Click(object sender, EventArgs e)
        {

        }

        private void btn_userinfo_Click(object sender, EventArgs e)
        {
            OpenChildForm(new StudentInfo());

        }

        private void panel_left_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btn_event_Click(object sender, EventArgs e)
        {
            ShowMenu(panel_event);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            OpenChildForm(new DeletedEvent());
            HideMenu();
        }

        private void btn_nhacevent_Click(object sender, EventArgs e)
        {
            OpenChildForm(new EventReminder());
            HideMenu();
        }

        private void panel_body_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel_logo_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btn_danhsachevent_Click(object sender, EventArgs e)
        {
            OpenChildForm(new EventList());
            HideMenu();
        }

        private void panel_logo_Paint_1(object sender, PaintEventArgs e)
        {

        }

        private void MainFormSinhVien_Load(object sender, EventArgs e)
        {

        }
        private void btn_themevent_Click(object sender, EventArgs e)
        {
            OpenChildForm(new Events());
            HideMenu();
        }

        private void btn_timetable_Click(object sender, EventArgs e)
        {
            ShowMenu(panel_timetable);
        }

        private void btn_themtkb_Click(object sender, EventArgs e)
        {
            OpenChildForm(new Timetable());
            HideMenu();
        }

        private void btn_bangtkb_Click(object sender, EventArgs e)
        {
            OpenChildForm(new TimetableBoard());
            HideMenu();
        }

        private void btn_planner_Click(object sender, EventArgs e)
        {
           
        }

        private void btn_option_Click(object sender, EventArgs e)
        {
            ShowMenu(panel_option);
        }

        private void btn_thoat_Click(object sender, EventArgs e)
        {
            HideMenu();
        }

        private void btn_dangxuat_Click(object sender, EventArgs e)
        {
            HideMenu();
        }
    }
}
