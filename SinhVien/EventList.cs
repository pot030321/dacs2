﻿using DACS.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DACS.SinhVien
{
    public partial class EventList : Form
    {
        public EventList()
        {
            InitializeComponent();
            this.Load += new System.EventHandler(this.EventList_Load);


        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btn_themevent_Click(object sender, EventArgs e)
        {
            Form f = new Events();
            f.Show();
            this.Hide();
        }
        public void FillEventGrid()
        {
            using (var context = new AppDbContext())
            {
                // Lấy danh sách sự kiện từ cơ sở dữ liệu
                var events = context.Events.ToList();

                // Set DataSource cho DataGridView
                dgvEvents.DataSource = events;

                // Cập nhật lại DataGridView
                dgvEvents.Refresh();

            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        private void EventList_Load(object sender, EventArgs e)
        {
            FillEventGrid();
        }


        private void btn_suaevent_Click(object sender, EventArgs e)
        {
            if (dgvEvents.SelectedRows.Count > 0)
            {
                int event_id = Convert.ToInt32(dgvEvents.SelectedRows[0].Cells["EventId"].Value);
                string name_event = dgvEvents.SelectedRows[0].Cells["EvName"].Value.ToString();
                int evTypeId = Convert.ToInt32(dgvEvents.SelectedRows[0].Cells["EventTypeId"].Value);
                DateTime Time_Start = Convert.ToDateTime(dgvEvents.SelectedRows[0].Cells["TimeStart"].Value);
                DateTime Time_End = Convert.ToDateTime(dgvEvents.SelectedRows[0].Cells["TimeEnd"].Value);
                string note = dgvEvents.SelectedRows[0].Cells["Note"].Value.ToString();
                int priority = Convert.ToInt32(dgvEvents.SelectedRows[0].Cells["Priority"].Value);

                Events eventForm = new Events();
                eventForm.UpdateEvent(event_id, name_event, evTypeId, Time_Start, Time_End, note, priority);
                eventForm.ShowDialog();

                // Sau khi form Events đóng, cập nhật lại DataGridView
                FillEventGrid();
            }
            else
            {
                MessageBox.Show("Vui lòng chọn một sự kiện để sửa!");
            }

            dgvEvents.Refresh();
        }
    }
}


        
    
    

