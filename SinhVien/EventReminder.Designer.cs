﻿namespace DACS.SinhVien
{
    partial class EventReminder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            label1 = new Label();
            label2 = new Label();
            label3 = new Label();
            label9 = new Label();
            label4 = new Label();
            label5 = new Label();
            label6 = new Label();
            label7 = new Label();
            label8 = new Label();
            txt_maeventsaptoi = new TextBox();
            txt_teneventsaptoi = new TextBox();
            txt_timesatartsaptoi = new TextBox();
            txt_timeendsaptoi = new TextBox();
            txt_loaieventsaptoi = new TextBox();
            txt_douutiensaptoi = new TextBox();
            label10 = new Label();
            txt_notesaptoi = new TextBox();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Times New Roman", 19.8000011F, FontStyle.Bold, GraphicsUnit.Point, 163);
            label1.Location = new Point(439, 9);
            label1.Name = "label1";
            label1.Size = new Size(174, 38);
            label1.TabIndex = 0;
            label1.Text = "Thông báo";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Times New Roman", 19.8000011F, FontStyle.Bold, GraphicsUnit.Point, 163);
            label2.Location = new Point(359, 61);
            label2.Name = "label2";
            label2.Size = new Size(336, 38);
            label2.TabIndex = 1;
            label2.Text = "Bạn có sự kiện sắp tới";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(129, 146);
            label3.Name = "label3";
            label3.Size = new Size(0, 20);
            label3.TabIndex = 2;
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Font = new Font("Times New Roman", 12F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label9.Location = new Point(95, 144);
            label9.Name = "label9";
            label9.Size = new Size(103, 22);
            label9.TabIndex = 19;
            label9.Text = "Mã sự kiện ";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new Font("Times New Roman", 12F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label4.Location = new Point(95, 194);
            label4.Name = "label4";
            label4.Size = new Size(107, 22);
            label4.TabIndex = 20;
            label4.Text = "Tên sự kiện ";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new Font("Times New Roman", 12F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label5.Location = new Point(95, 246);
            label5.Name = "label5";
            label5.Size = new Size(108, 22);
            label5.TabIndex = 21;
            label5.Text = "Giờ bắt đầu ";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new Font("Times New Roman", 12F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label6.Location = new Point(95, 297);
            label6.Name = "label6";
            label6.Size = new Size(107, 22);
            label6.TabIndex = 22;
            label6.Text = "Giờ kết thúc";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Font = new Font("Times New Roman", 12F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label7.Location = new Point(95, 339);
            label7.Name = "label7";
            label7.Size = new Size(108, 22);
            label7.TabIndex = 23;
            label7.Text = "Loại sự kiện";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Font = new Font("Times New Roman", 12F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label8.Location = new Point(95, 389);
            label8.Name = "label8";
            label8.Size = new Size(98, 22);
            label8.TabIndex = 24;
            label8.Text = "Độ ưu tiên ";
            // 
            // txt_maeventsaptoi
            // 
            txt_maeventsaptoi.Location = new Point(204, 143);
            txt_maeventsaptoi.Name = "txt_maeventsaptoi";
            txt_maeventsaptoi.ReadOnly = true;
            txt_maeventsaptoi.Size = new Size(680, 27);
            txt_maeventsaptoi.TabIndex = 25;
            // 
            // txt_teneventsaptoi
            // 
            txt_teneventsaptoi.Location = new Point(204, 194);
            txt_teneventsaptoi.Name = "txt_teneventsaptoi";
            txt_teneventsaptoi.ReadOnly = true;
            txt_teneventsaptoi.Size = new Size(680, 27);
            txt_teneventsaptoi.TabIndex = 26;
            // 
            // txt_timesatartsaptoi
            // 
            txt_timesatartsaptoi.Location = new Point(204, 244);
            txt_timesatartsaptoi.Name = "txt_timesatartsaptoi";
            txt_timesatartsaptoi.ReadOnly = true;
            txt_timesatartsaptoi.Size = new Size(680, 27);
            txt_timesatartsaptoi.TabIndex = 27;
            // 
            // txt_timeendsaptoi
            // 
            txt_timeendsaptoi.Location = new Point(204, 295);
            txt_timeendsaptoi.Name = "txt_timeendsaptoi";
            txt_timeendsaptoi.ReadOnly = true;
            txt_timeendsaptoi.Size = new Size(680, 27);
            txt_timeendsaptoi.TabIndex = 28;
            // 
            // txt_loaieventsaptoi
            // 
            txt_loaieventsaptoi.Location = new Point(204, 339);
            txt_loaieventsaptoi.Name = "txt_loaieventsaptoi";
            txt_loaieventsaptoi.ReadOnly = true;
            txt_loaieventsaptoi.Size = new Size(680, 27);
            txt_loaieventsaptoi.TabIndex = 29;
            // 
            // txt_douutiensaptoi
            // 
            txt_douutiensaptoi.Location = new Point(204, 389);
            txt_douutiensaptoi.Name = "txt_douutiensaptoi";
            txt_douutiensaptoi.ReadOnly = true;
            txt_douutiensaptoi.Size = new Size(680, 27);
            txt_douutiensaptoi.TabIndex = 30;
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Font = new Font("Times New Roman", 12F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label10.Location = new Point(95, 440);
            label10.Name = "label10";
            label10.Size = new Size(72, 22);
            label10.TabIndex = 31;
            label10.Text = "Ghi chú";
            // 
            // txt_notesaptoi
            // 
            txt_notesaptoi.Location = new Point(204, 440);
            txt_notesaptoi.Name = "txt_notesaptoi";
            txt_notesaptoi.ReadOnly = true;
            txt_notesaptoi.Size = new Size(680, 27);
            txt_notesaptoi.TabIndex = 32;
            // 
            // EventReminder
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1042, 508);
            Controls.Add(txt_notesaptoi);
            Controls.Add(label10);
            Controls.Add(txt_douutiensaptoi);
            Controls.Add(txt_loaieventsaptoi);
            Controls.Add(txt_timeendsaptoi);
            Controls.Add(txt_timesatartsaptoi);
            Controls.Add(txt_teneventsaptoi);
            Controls.Add(txt_maeventsaptoi);
            Controls.Add(label8);
            Controls.Add(label7);
            Controls.Add(label6);
            Controls.Add(label5);
            Controls.Add(label4);
            Controls.Add(label9);
            Controls.Add(label3);
            Controls.Add(label2);
            Controls.Add(label1);
            Name = "EventReminder";
            Text = "EventReminder";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label label1;
        private Label label2;
        private Label label3;
        private Label label9;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private TextBox txt_maeventsaptoi;
        private TextBox txt_teneventsaptoi;
        private TextBox txt_timesatartsaptoi;
        private TextBox txt_timeendsaptoi;
        private TextBox txt_loaieventsaptoi;
        private TextBox txt_douutiensaptoi;
        private Label label10;
        private TextBox txt_notesaptoi;
    }
}