﻿namespace DACS.Admin
{
    partial class Class
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            label1 = new Label();
            label2 = new Label();
            txt_malop = new TextBox();
            label3 = new Label();
            label4 = new Label();
            label5 = new Label();
            label6 = new Label();
            txt_mamon = new TextBox();
            txt_manganh = new TextBox();
            txt_matruong = new TextBox();
            txt_tinhtrang = new TextBox();
            btn_tinhtrang = new Button();
            label7 = new Label();
            txt_timestart = new TextBox();
            label8 = new Label();
            txt_timend = new TextBox();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Times New Roman", 19.8000011F, FontStyle.Bold, GraphicsUnit.Point, 163);
            label1.Location = new Point(430, 18);
            label1.Name = "label1";
            label1.Size = new Size(219, 38);
            label1.TabIndex = 0;
            label1.Text = "Thêm lớp học";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Times New Roman", 13.8F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label2.Location = new Point(101, 73);
            label2.Name = "label2";
            label2.Size = new Size(77, 26);
            label2.TabIndex = 1;
            label2.Text = "Mã lớp";
            // 
            // txt_malop
            // 
            txt_malop.Location = new Point(227, 72);
            txt_malop.Name = "txt_malop";
            txt_malop.Size = new Size(716, 27);
            txt_malop.TabIndex = 2;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new Font("Times New Roman", 13.8F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label3.Location = new Point(101, 125);
            label3.Name = "label3";
            label3.Size = new Size(89, 26);
            label3.TabIndex = 3;
            label3.Text = "Mã môn";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new Font("Times New Roman", 13.8F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label4.Location = new Point(101, 173);
            label4.Name = "label4";
            label4.Size = new Size(104, 26);
            label4.TabIndex = 4;
            label4.Text = "Mã ngành";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new Font("Times New Roman", 13.8F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label5.Location = new Point(101, 224);
            label5.Name = "label5";
            label5.Size = new Size(108, 26);
            label5.TabIndex = 5;
            label5.Text = "Mã trường";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new Font("Times New Roman", 13.8F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label6.Location = new Point(101, 376);
            label6.Name = "label6";
            label6.Size = new Size(109, 26);
            label6.TabIndex = 6;
            label6.Text = "Tình trạng";
            // 
            // txt_mamon
            // 
            txt_mamon.Location = new Point(227, 124);
            txt_mamon.Name = "txt_mamon";
            txt_mamon.Size = new Size(716, 27);
            txt_mamon.TabIndex = 7;
            // 
            // txt_manganh
            // 
            txt_manganh.Location = new Point(227, 172);
            txt_manganh.Name = "txt_manganh";
            txt_manganh.Size = new Size(716, 27);
            txt_manganh.TabIndex = 8;
            // 
            // txt_matruong
            // 
            txt_matruong.Location = new Point(227, 223);
            txt_matruong.Name = "txt_matruong";
            txt_matruong.Size = new Size(716, 27);
            txt_matruong.TabIndex = 9;
            // 
            // txt_tinhtrang
            // 
            txt_tinhtrang.Location = new Point(227, 376);
            txt_tinhtrang.Name = "txt_tinhtrang";
            txt_tinhtrang.Size = new Size(716, 27);
            txt_tinhtrang.TabIndex = 10;
            // 
            // btn_tinhtrang
            // 
            btn_tinhtrang.Location = new Point(475, 441);
            btn_tinhtrang.Name = "btn_tinhtrang";
            btn_tinhtrang.Size = new Size(94, 29);
            btn_tinhtrang.TabIndex = 11;
            btn_tinhtrang.Text = "Thêm ";
            btn_tinhtrang.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Font = new Font("Times New Roman", 13.8F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label7.Location = new Point(101, 278);
            label7.Name = "label7";
            label7.Size = new Size(120, 26);
            label7.TabIndex = 12;
            label7.Text = "Giờ bắt đầu";
            // 
            // txt_timestart
            // 
            txt_timestart.Location = new Point(227, 279);
            txt_timestart.Name = "txt_timestart";
            txt_timestart.Size = new Size(716, 27);
            txt_timestart.TabIndex = 13;
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Font = new Font("Times New Roman", 13.8F, FontStyle.Regular, GraphicsUnit.Point, 163);
            label8.Location = new Point(101, 328);
            label8.Name = "label8";
            label8.Size = new Size(126, 26);
            label8.TabIndex = 14;
            label8.Text = "Giờ kết thúc";
            // 
            // txt_timend
            // 
            txt_timend.Location = new Point(227, 328);
            txt_timend.Name = "txt_timend";
            txt_timend.Size = new Size(716, 27);
            txt_timend.TabIndex = 15;
            // 
            // Class
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1061, 497);
            Controls.Add(txt_timend);
            Controls.Add(label8);
            Controls.Add(txt_timestart);
            Controls.Add(label7);
            Controls.Add(btn_tinhtrang);
            Controls.Add(txt_tinhtrang);
            Controls.Add(txt_matruong);
            Controls.Add(txt_manganh);
            Controls.Add(txt_mamon);
            Controls.Add(label6);
            Controls.Add(label5);
            Controls.Add(label4);
            Controls.Add(label3);
            Controls.Add(txt_malop);
            Controls.Add(label2);
            Controls.Add(label1);
            Name = "Class";
            Text = "Class";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label label1;
        private Label label2;
        private TextBox txt_malop;
        private Label label3;
        private Label label4;
        private Label label5;
        private Label label6;
        private TextBox txt_mamon;
        private TextBox txt_manganh;
        private TextBox txt_matruong;
        private TextBox txt_tinhtrang;
        private Button btn_tinhtrang;
        private Label label7;
        private TextBox txt_timestart;
        private Label label8;
        private TextBox txt_timend;
    }
}