﻿namespace DACS.Admin
{
    partial class MainFormAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            panel_left = new Panel();
            btn_thoat = new Button();
            btn_listuser = new Button();
            panel_class = new Panel();
            button3 = new Button();
            button2 = new Button();
            button1 = new Button();
            panel_logo = new Panel();
            panel_body = new Panel();
            panel_left.SuspendLayout();
            panel_class.SuspendLayout();
            SuspendLayout();
            // 
            // panel_left
            // 
            panel_left.BackColor = SystemColors.ActiveCaption;
            panel_left.Controls.Add(btn_thoat);
            panel_left.Controls.Add(btn_listuser);
            panel_left.Controls.Add(panel_class);
            panel_left.Controls.Add(button1);
            panel_left.Controls.Add(panel_logo);
            panel_left.Dock = DockStyle.Left;
            panel_left.Location = new Point(0, 0);
            panel_left.Name = "panel_left";
            panel_left.Size = new Size(201, 261);
            panel_left.TabIndex = 0;
            // 
            // btn_thoat
            // 
            btn_thoat.BackColor = SystemColors.ActiveCaption;
            btn_thoat.Dock = DockStyle.Top;
            btn_thoat.FlatAppearance.BorderSize = 0;
            btn_thoat.FlatStyle = FlatStyle.Flat;
            btn_thoat.Font = new Font("Times New Roman", 10.2F, FontStyle.Bold, GraphicsUnit.Point, 163);
            btn_thoat.Location = new Point(0, 172);
            btn_thoat.Name = "btn_thoat";
            btn_thoat.Size = new Size(201, 29);
            btn_thoat.TabIndex = 1;
            btn_thoat.Text = "Thoát";
            btn_thoat.UseVisualStyleBackColor = false;
            // 
            // btn_listuser
            // 
            btn_listuser.BackColor = SystemColors.ActiveCaption;
            btn_listuser.Dock = DockStyle.Top;
            btn_listuser.FlatAppearance.BorderSize = 0;
            btn_listuser.FlatStyle = FlatStyle.Flat;
            btn_listuser.Font = new Font("Times New Roman", 10.2F, FontStyle.Bold, GraphicsUnit.Point, 163);
            btn_listuser.Location = new Point(0, 143);
            btn_listuser.Name = "btn_listuser";
            btn_listuser.Size = new Size(201, 29);
            btn_listuser.TabIndex = 1;
            btn_listuser.Text = "Danh sách người dùng";
            btn_listuser.UseVisualStyleBackColor = false;
            btn_listuser.Click += btn_listuser_Click;
            // 
            // panel_class
            // 
            panel_class.Controls.Add(button3);
            panel_class.Controls.Add(button2);
            panel_class.Dock = DockStyle.Top;
            panel_class.Location = new Point(0, 87);
            panel_class.Name = "panel_class";
            panel_class.Size = new Size(201, 56);
            panel_class.TabIndex = 1;
            // 
            // button3
            // 
            button3.Dock = DockStyle.Top;
            button3.FlatAppearance.BorderSize = 0;
            button3.FlatStyle = FlatStyle.Flat;
            button3.Font = new Font("Times New Roman", 10.2F);
            button3.Location = new Point(0, 29);
            button3.Name = "button3";
            button3.Size = new Size(201, 30);
            button3.TabIndex = 3;
            button3.Text = "Danh sách lớp học";
            button3.UseVisualStyleBackColor = true;
            button3.Click += button3_Click;
            // 
            // button2
            // 
            button2.Dock = DockStyle.Top;
            button2.FlatAppearance.BorderSize = 0;
            button2.FlatStyle = FlatStyle.Flat;
            button2.Font = new Font("Times New Roman", 10.2F);
            button2.Location = new Point(0, 0);
            button2.Name = "button2";
            button2.Size = new Size(201, 29);
            button2.TabIndex = 2;
            button2.Text = "Thêm lớp học";
            button2.UseVisualStyleBackColor = true;
            button2.Click += button2_Click;
            // 
            // button1
            // 
            button1.AutoEllipsis = true;
            button1.BackColor = SystemColors.ActiveCaption;
            button1.Dock = DockStyle.Top;
            button1.FlatAppearance.BorderSize = 0;
            button1.FlatStyle = FlatStyle.Flat;
            button1.Font = new Font("Times New Roman", 10.2F, FontStyle.Bold, GraphicsUnit.Point, 163);
            button1.Location = new Point(0, 58);
            button1.Name = "button1";
            button1.Size = new Size(201, 29);
            button1.TabIndex = 1;
            button1.Text = "Lớp";
            button1.UseVisualStyleBackColor = false;
            button1.Click += button1_Click;
            // 
            // panel_logo
            // 
            panel_logo.Dock = DockStyle.Top;
            panel_logo.Location = new Point(0, 0);
            panel_logo.Name = "panel_logo";
            panel_logo.Size = new Size(201, 58);
            panel_logo.TabIndex = 1;
            // 
            // panel_body
            // 
            panel_body.Dock = DockStyle.Fill;
            panel_body.Location = new Point(201, 0);
            panel_body.Name = "panel_body";
            panel_body.Size = new Size(654, 261);
            panel_body.TabIndex = 1;
            // 
            // MainFormAdmin
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            AutoScroll = true;
            ClientSize = new Size(855, 261);
            Controls.Add(panel_body);
            Controls.Add(panel_left);
            Name = "MainFormAdmin";
            Text = "MainFormAdmin";
            Load += MainFormAdmin_Load;
            panel_left.ResumeLayout(false);
            panel_class.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private Panel panel_left;
        private Button button1;
        private Panel panel_logo;
        private Panel panel_class;
        private Button button3;
        private Button button2;
        private Button btn_thoat;
        private Button btn_listuser;
        private Panel panel_body;
    }
}