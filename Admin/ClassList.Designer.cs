﻿namespace DACS.Admin
{
    partial class ClassList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            label1 = new Label();
            dataGridView1 = new DataGridView();
            txt_malop = new DataGridViewTextBoxColumn();
            txt_mamon = new DataGridViewTextBoxColumn();
            txt_manganh = new DataGridViewTextBoxColumn();
            txt_matruong = new DataGridViewTextBoxColumn();
            txt_timestart = new DataGridViewTextBoxColumn();
            txt_timeend = new DataGridViewTextBoxColumn();
            txt_tinhtrang = new DataGridViewTextBoxColumn();
            btn_sualop = new Button();
            btn_xoalop = new Button();
            btn_timkiemlop = new Button();
            txt_timkiemlop = new TextBox();
            btn_themlop = new Button();
            ((System.ComponentModel.ISupportInitialize)dataGridView1).BeginInit();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Times New Roman", 19.8000011F, FontStyle.Bold, GraphicsUnit.Point, 163);
            label1.Location = new Point(409, 27);
            label1.Name = "label1";
            label1.Size = new Size(286, 38);
            label1.TabIndex = 0;
            label1.Text = "Danh sách lớp học";
            // 
            // dataGridView1
            // 
            dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridView1.Columns.AddRange(new DataGridViewColumn[] { txt_malop, txt_mamon, txt_manganh, txt_matruong, txt_timestart, txt_timeend, txt_tinhtrang });
            dataGridView1.Location = new Point(70, 137);
            dataGridView1.Name = "dataGridView1";
            dataGridView1.RowHeadersWidth = 51;
            dataGridView1.Size = new Size(929, 259);
            dataGridView1.TabIndex = 2;
            // 
            // txt_malop
            // 
            txt_malop.HeaderText = "Mã lớp";
            txt_malop.MinimumWidth = 6;
            txt_malop.Name = "txt_malop";
            txt_malop.Width = 125;
            // 
            // txt_mamon
            // 
            txt_mamon.HeaderText = "Mã môn";
            txt_mamon.MinimumWidth = 6;
            txt_mamon.Name = "txt_mamon";
            txt_mamon.Width = 125;
            // 
            // txt_manganh
            // 
            txt_manganh.HeaderText = "Mã ngành";
            txt_manganh.MinimumWidth = 6;
            txt_manganh.Name = "txt_manganh";
            txt_manganh.Width = 125;
            // 
            // txt_matruong
            // 
            txt_matruong.HeaderText = "Mã trường";
            txt_matruong.MinimumWidth = 6;
            txt_matruong.Name = "txt_matruong";
            txt_matruong.Width = 125;
            // 
            // txt_timestart
            // 
            txt_timestart.HeaderText = "Giờ bắt đầu";
            txt_timestart.MinimumWidth = 6;
            txt_timestart.Name = "txt_timestart";
            txt_timestart.Width = 125;
            // 
            // txt_timeend
            // 
            txt_timeend.HeaderText = "Giờ kết thúc";
            txt_timeend.MinimumWidth = 6;
            txt_timeend.Name = "txt_timeend";
            txt_timeend.Width = 125;
            // 
            // txt_tinhtrang
            // 
            txt_tinhtrang.HeaderText = "Tình trạng";
            txt_tinhtrang.MinimumWidth = 6;
            txt_tinhtrang.Name = "txt_tinhtrang";
            txt_tinhtrang.Width = 125;
            // 
            // btn_sualop
            // 
            btn_sualop.Location = new Point(499, 412);
            btn_sualop.Name = "btn_sualop";
            btn_sualop.Size = new Size(94, 29);
            btn_sualop.TabIndex = 4;
            btn_sualop.Text = "Sửa";
            btn_sualop.UseVisualStyleBackColor = true;
            // 
            // btn_xoalop
            // 
            btn_xoalop.Location = new Point(905, 412);
            btn_xoalop.Name = "btn_xoalop";
            btn_xoalop.Size = new Size(94, 29);
            btn_xoalop.TabIndex = 5;
            btn_xoalop.Text = "Xóa";
            btn_xoalop.UseVisualStyleBackColor = true;
            // 
            // btn_timkiemlop
            // 
            btn_timkiemlop.Location = new Point(850, 84);
            btn_timkiemlop.Name = "btn_timkiemlop";
            btn_timkiemlop.Size = new Size(149, 29);
            btn_timkiemlop.TabIndex = 6;
            btn_timkiemlop.Text = "Tìm kiếm (Mã lớp)";
            btn_timkiemlop.UseVisualStyleBackColor = true;
            // 
            // txt_timkiemlop
            // 
            txt_timkiemlop.Location = new Point(668, 84);
            txt_timkiemlop.Name = "txt_timkiemlop";
            txt_timkiemlop.Size = new Size(176, 27);
            txt_timkiemlop.TabIndex = 7;
            // 
            // btn_themlop
            // 
            btn_themlop.Location = new Point(70, 412);
            btn_themlop.Name = "btn_themlop";
            btn_themlop.Size = new Size(94, 29);
            btn_themlop.TabIndex = 8;
            btn_themlop.Text = "Thêm";
            btn_themlop.UseVisualStyleBackColor = true;
            // 
            // ClassList
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1099, 522);
            Controls.Add(btn_themlop);
            Controls.Add(txt_timkiemlop);
            Controls.Add(btn_timkiemlop);
            Controls.Add(btn_xoalop);
            Controls.Add(btn_sualop);
            Controls.Add(dataGridView1);
            Controls.Add(label1);
            Name = "ClassList";
            Text = "ClassList";
            ((System.ComponentModel.ISupportInitialize)dataGridView1).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label label1;
        private DataGridView dataGridView1;
        private DataGridViewTextBoxColumn txt_malop;
        private DataGridViewTextBoxColumn txt_mamon;
        private DataGridViewTextBoxColumn txt_manganh;
        private DataGridViewTextBoxColumn txt_matruong;
        private DataGridViewTextBoxColumn txt_timestart;
        private DataGridViewTextBoxColumn txt_timeend;
        private DataGridViewTextBoxColumn txt_tinhtrang;
        private Button btn_sualop;
        private Button btn_xoalop;
        private Button btn_timkiemlop;
        private TextBox txt_timkiemlop;
        private Button btn_themlop;
    }
}