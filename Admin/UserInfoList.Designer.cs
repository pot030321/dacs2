﻿namespace DACS.Admin
{
    partial class UserInfoList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            label1 = new Label();
            dataGridView1 = new DataGridView();
            txt_idsv = new DataGridViewTextBoxColumn();
            txt_hotensv = new DataGridViewTextBoxColumn();
            txt_tuoisv = new DataGridViewTextBoxColumn();
            txt_truongsv = new DataGridViewTextBoxColumn();
            txt_machucvusv = new DataGridViewTextBoxColumn();
            btn_timkiemsv = new Button();
            txt_timkiemsv = new TextBox();
            btn_xoasv = new Button();
            btn_suasv = new Button();
            btn_themsv = new Button();
            ((System.ComponentModel.ISupportInitialize)dataGridView1).BeginInit();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Times New Roman", 19.8000011F, FontStyle.Bold, GraphicsUnit.Point, 163);
            label1.Location = new Point(343, 26);
            label1.Name = "label1";
            label1.Size = new Size(486, 38);
            label1.TabIndex = 0;
            label1.Text = "Danh sách thông tin người dùng";
            label1.Click += label1_Click;
            // 
            // dataGridView1
            // 
            dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridView1.Columns.AddRange(new DataGridViewColumn[] { txt_idsv, txt_hotensv, txt_tuoisv, txt_truongsv, txt_machucvusv });
            dataGridView1.Location = new Point(226, 140);
            dataGridView1.Name = "dataGridView1";
            dataGridView1.RowHeadersWidth = 51;
            dataGridView1.Size = new Size(679, 272);
            dataGridView1.TabIndex = 1;
            // 
            // txt_idsv
            // 
            txt_idsv.HeaderText = "ID";
            txt_idsv.MinimumWidth = 6;
            txt_idsv.Name = "txt_idsv";
            txt_idsv.Width = 125;
            // 
            // txt_hotensv
            // 
            txt_hotensv.HeaderText = "Họ và tên";
            txt_hotensv.MinimumWidth = 6;
            txt_hotensv.Name = "txt_hotensv";
            txt_hotensv.Width = 125;
            // 
            // txt_tuoisv
            // 
            txt_tuoisv.HeaderText = "Tuổi";
            txt_tuoisv.MinimumWidth = 6;
            txt_tuoisv.Name = "txt_tuoisv";
            txt_tuoisv.Width = 125;
            // 
            // txt_truongsv
            // 
            txt_truongsv.HeaderText = "Trường";
            txt_truongsv.MinimumWidth = 6;
            txt_truongsv.Name = "txt_truongsv";
            txt_truongsv.Width = 125;
            // 
            // txt_machucvusv
            // 
            txt_machucvusv.HeaderText = "Mã chức vụ";
            txt_machucvusv.MinimumWidth = 6;
            txt_machucvusv.Name = "txt_machucvusv";
            txt_machucvusv.Width = 125;
            // 
            // btn_timkiemsv
            // 
            btn_timkiemsv.Location = new Point(826, 88);
            btn_timkiemsv.Name = "btn_timkiemsv";
            btn_timkiemsv.Size = new Size(79, 29);
            btn_timkiemsv.TabIndex = 5;
            btn_timkiemsv.Text = "Tìm kiếm";
            btn_timkiemsv.UseVisualStyleBackColor = true;
            // 
            // txt_timkiemsv
            // 
            txt_timkiemsv.Location = new Point(635, 88);
            txt_timkiemsv.Name = "txt_timkiemsv";
            txt_timkiemsv.Size = new Size(176, 27);
            txt_timkiemsv.TabIndex = 6;
            // 
            // btn_xoasv
            // 
            btn_xoasv.Location = new Point(811, 439);
            btn_xoasv.Name = "btn_xoasv";
            btn_xoasv.Size = new Size(94, 29);
            btn_xoasv.TabIndex = 4;
            btn_xoasv.Text = "Xóa";
            btn_xoasv.UseVisualStyleBackColor = true;
            // 
            // btn_suasv
            // 
            btn_suasv.Location = new Point(509, 439);
            btn_suasv.Name = "btn_suasv";
            btn_suasv.Size = new Size(94, 29);
            btn_suasv.TabIndex = 3;
            btn_suasv.Text = "Sửa";
            btn_suasv.UseVisualStyleBackColor = true;
            // 
            // btn_themsv
            // 
            btn_themsv.Location = new Point(227, 439);
            btn_themsv.Name = "btn_themsv";
            btn_themsv.Size = new Size(117, 29);
            btn_themsv.TabIndex = 2;
            btn_themsv.Text = "Thêm ";
            btn_themsv.UseVisualStyleBackColor = true;
            // 
            // UserInfoList
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1145, 537);
            Controls.Add(txt_timkiemsv);
            Controls.Add(btn_timkiemsv);
            Controls.Add(btn_xoasv);
            Controls.Add(btn_suasv);
            Controls.Add(btn_themsv);
            Controls.Add(dataGridView1);
            Controls.Add(label1);
            Name = "UserInfoList";
            Text = "UserList";
            ((System.ComponentModel.ISupportInitialize)dataGridView1).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label label1;
        private DataGridView dataGridView1;
        private Button btn_timkiemsv;
        private TextBox txt_timkiemsv;
        private Button btn_xoasv;
        private Button btn_suasv;
        private Button btn_themsv;
        private DataGridViewTextBoxColumn txt_idsv;
        private DataGridViewTextBoxColumn txt_hotensv;
        private DataGridViewTextBoxColumn txt_tuoisv;
        private DataGridViewTextBoxColumn txt_truongsv;
        private DataGridViewTextBoxColumn txt_machucvusv;
    }
}