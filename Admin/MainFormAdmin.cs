﻿using DACS.SinhVien;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DACS.Admin
{
    public partial class MainFormAdmin : Form
    {
        public MainFormAdmin()
        {
            InitializeComponent();
            CustomizeMenu();
        }
        private Form currentFormChild;
        private void OpenChildForm(Form childForm)
        {
            if (currentFormChild != null)
            {
                currentFormChild.Close();
            }
            currentFormChild = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            panel_body.Controls.Add(childForm);
            panel_body.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
        }
        private void CustomizeMenu()
        {
            panel_class.Visible = false;

        }
        private void HideMenu()
        {
            if (panel_class.Visible == true)
                panel_class.Visible = false;

        }
        private void ShowMenu(Panel SubMenu)
        {
            if (SubMenu.Visible == false)
            {
                HideMenu();
                SubMenu.Visible = true;
            }
            else
                SubMenu.Visible = false;
        }
        private void MainFormAdmin_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            ShowMenu(panel_class);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenChildForm(new Class());
            HideMenu();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            OpenChildForm(new ClassList());
            
        }

        private void btn_listuser_Click(object sender, EventArgs e)
        {
            OpenChildForm(new UserInfoList());  
        }
    }
}
