﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DACS.Models
{
    public class SubJect
    {
        [Key]
        public int SubJectId { get; set; }
        public String? SubJectName { get; set; }
        public int FacultyId { get; set; }
        public int SchoolId { get; set; }
     

    }
}
