﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DACS.Models
{
    public class StudentInGroup
    {
        [Key]
        public int ? GroupId {  get; set; }
        public int ?UserId { get; set; }
        public int ? SchoolId { get; set; }
        public int ?RoleId { get; set; }
      
    }
}
