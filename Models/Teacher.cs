﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DACS.Models
{
    public class Teacher
    {
        [Key]
        public int TeacherId { get; set; }
        public String? TeacherName { get; set; }
        public  String ? Academic_Qualifications {  get; set; }
        public int FacultyId { get; set; }
        public int SchoolId { get; set; }
        public int Phone { get; set; }
        public String ?Mail { get; set; }
   
    }
}
