﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DACS.Models
{
    public class EventType
    {
        [Key]
        public int EventId { get; set; }
        public string ? EventTypeName { get; set; }
    }
}
