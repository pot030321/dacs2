﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DACS.Models
{
    public class TimeClass
    {
        [Key]
        public int ClassSubId { get; set; }
        public string ?SubName {  get; set; }
        public DateTime? TimeStart { get; set; }
        public DateTime? TimeEnd { get; set; }
        public int SubJectId { get; set; }
        public int Class {  get; set; }
        public int teacherId { get; set; }
      
    }
}
