﻿using Microsoft.Extensions.Logging;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Linq;

namespace DACS.Models
{
    public class Event
    {
        [Key]
        public int EventId { get; set; }

        public int EventTypeId { get; set; }

        [Required]
        [StringLength(255)]
        public string? EvName { get; set; }

        [Required]
        public DateTime? TimeStart { get; set; }

        public DateTime? TimeEnd { get; set; }

        public int Priority { get; set; }

        public string? Note { get; set; }

      
    }





}
