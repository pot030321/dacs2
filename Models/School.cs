﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DACS.Models
{
    public class School
    {
        
        public int SchoolId {  get; set; }
        public string ?SchoolName { get; set; }
    }
}
