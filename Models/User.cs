﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
namespace DACS.Models
{
    public class User
    {
        [Key]
        public int UserID { get; set; }
        public string? Name { get; set; }
        public int? Age { get; set; }
        public bool Delete { get; set; }
        public string ?SN { get; set; }
        public int RoleId { get; set; }
       
    }
}
