﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DACS.Models
{
    public class ClassSubJect
    {
        [Key]
        public int ClassSubId { get; set; }
        public int SubJectId { get; set; }
        public int FacultyId { get; set; }
        public int SchoolId { get; set; }
      
        public int GroupId { get; set; }
     
        public int Status { get; set; }
       

    }
}
