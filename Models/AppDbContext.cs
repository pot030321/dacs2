﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;

namespace DACS.Models
{
    public class AppDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Faculty> Faculties { get; set; }
        public DbSet<GroupStudent> GroupStudents { get; set; }
        public DbSet<SubJect> SubJects { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<TimeClass> TimeClasses { get; set; }
        public DbSet<StudentInGroup> StudentInGroups { get; set; }
        public DbSet<EventType> EventTypes { get; set; }
        public DbSet<ClassSubJect> ClassSubJects { get; set; }
        public DbSet<School> Schools { get; set; }
      


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=21AK22-COM\SQLEXPRESS;Database=DACS;User Id=sa;Password=123456;TrustServerCertificate=true;");

        }
    }

}
